  var AATKit = exports;

    var cordova = require('cordova');

    // base
    AATKit.initWithConfiguration = function(configuration){
        cordova.exec(null, null, "AATKitCordovaPlugin", "initWithConfiguration", [configuration]);
    };
    AATKit.reconfigure = function(configuration){
        cordova.exec(null, null, "AATKitCordovaPlugin", "reconfigure", [configuration]);
    };
    AATKit.getVersion = function(resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "getVersion", []);
    };
    AATKit.showConsentDialogIfNeeded = function(){
        cordova.exec(null, null, "AATKitCordovaPlugin", "showConsentDialogIfNeeded", []);
    };
    AATKit.editConsent = function(){
        cordova.exec(null, null, "AATKitCordovaPlugin", "editConsent", []);
    };
    AATKit.reloadConsent = function(){
        cordova.exec(null, null, "AATKitCordovaPlugin", "reloadConsent", []);
    };
    AATKit.setDebugEnabled = function(enabled){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setDebugEnabled", [enabled]);
    };
    AATKit.setDebugShakeEnabled = function(enabled){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setDebugShakeEnabled", [enabled]);
    };
    AATKit.configureDebugScreen = function(configuration){
        cordova.exec(null, null, "AATKitCordovaPlugin", "configureDebugScreen", [configuration]);
    };
    AATKit.getDebugInfo = function(resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "getDebugInfo", []);
    };
    AATKit.setIsChildDirected = function(enabled){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setIsChildDirected", [enabled]);
    };
    AATKit.isConsentOptIn = function(resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "isConsentOptIn", []);
    };

    // networks
    AATKit.setNetworkEnabled = function(networkName, enabled){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setNetworkEnabled", [networkName, enabled]);
    };
    AATKit.isNetworkEnabled = function(networkName, resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "isNetworkEnabled", [networkName]);
    };

    // placements
    AATKit.createPlacement = function(placementName, placementSize){
        cordova.exec(null, null, "AATKitCordovaPlugin", "createPlacement", [placementName, placementSize])
    };
    AATKit.createRewardedVideoPlacement = function(placementName){
        cordova.exec(null, null, "AATKitCordovaPlugin", "createRewardedVideoPlacement", [placementName])
    };
    AATKit.createAppOpenAdPlacement = function(placementName){
        cordova.exec(null, null, "AATKitCordovaPlugin", "createAppOpenAdPlacement", [placementName])
    };
    AATKit.reloadPlacement = function(placementName, resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "reloadPlacement", [placementName])
    };
    AATKit.reloadPlacementForced = function(placementName, forced, resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "reloadPlacementForced", [placementName, forced])
    };
    AATKit.startPlacementAutoReload = function(placementName){
        cordova.exec(null, null, "AATKitCordovaPlugin", "startPlacementAutoReload", [placementName])
    };
    AATKit.stopPlacementAutoReload = function(placementName){
        cordova.exec(null, null, "AATKitCordovaPlugin", "stopPlacementAutoReload", [placementName])
    };
    AATKit.hasAdForPlacement = function(placementName, resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "hasAdForPlacement", [placementName]);
    };
    AATKit.isFrequencyCapReachedForPlacement = function(placementName, resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "isFrequencyCapReachedForPlacement", [placementName]);
    };
    AATKit.muteVideoAds = function(mute){
        cordova.exec(null, null, "AATKitCordovaPlugin", "muteVideoAds", [mute]);
    };
    AATKit.setImpressionListener = function(placementName){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setImpressionListener", [placementName])
    };
    AATKit.setPublisherProvidedId = function(providedId){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setPublisherProvidedId", [providedId])
    };
    AATKit.setCollapsibleBannerOptions = function(placementName, options){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setCollapsibleBannerOptions", [placementName, options]);
    };

    //banners
    AATKit.addPlacementToView = function(placementName){
        cordova.exec(null, null, "AATKitCordovaPlugin", "addPlacementToView", [placementName]);
    };

    AATKit.removePlacementFromView = function(placementName){
        cordova.exec(null, null, "AATKitCordovaPlugin", "removePlacementFromView", [placementName]);
    };

    AATKit.setPlacementAlignment = function(placementName, alignment){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setPlacementAlignment", [placementName, alignment]);
    };
               
    AATKit.setPlacementAlignmentWithOffset = function(placementName, alignment, x, y){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setPlacementAlignmentWithOffset", [placementName, alignment, x, y]);
    };

    AATKit.setPlacementPosition = function(placementName, x, y){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setPlacementPosition", [placementName, x, y]);
    };

    AATKit.setPlacementContentGravity = function(placementName, gravity){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setPlacementContentGravity", [placementName, gravity]);
    };

    //fullscreen
    AATKit.showPlacement = function(placementName, resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "showPlacement", [placementName]);
    };

    // targeting
    AATKit.addAdNetworkForKeywordTargeting = function(networkName){
        cordova.exec(null, null, "AATKitCordovaPlugin", "addAdNetworkForKeywordTargeting", [networkName]);
    };
    AATKit.removeAdNetworkForKeywordTargeting = function(networkName){
        cordova.exec(null, null, "AATKitCordovaPlugin", "removeAdNetworkForKeywordTargeting", [networkName]);
    };
    AATKit.setTargetingInfo = function(targetingInfo){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setTargetingInfo", [targetingInfo]);
    };
    AATKit.setTargetingInfoForPlacement = function(placementName, targetingInfo){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setTargetingInfoForPlacement", [placementName, targetingInfo]);
    };
    AATKit.setContentTargetingUrl = function(url){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setContentTargetingUrl", [url]);
    };
    AATKit.setContentTargetingUrlForPlacement = function(placementName, url){
        cordova.exec(null, null, "AATKitCordovaPlugin", "setContentTargetingUrlForPlacement", [placementName, url]);
    };

    //device size
    AATKit.isTablet = function(resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "isTablet", []);
    };

    AATKit.maximumBannerSizePortrait = function(resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "maximumBannerSizePortrait", []);
    };

    AATKit.maximumBannerSizeLandscape = function(resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "maximumBannerSizeLandscape", []);
    };

    AATKit.fittingBannerSizesPortrait = function(resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "fittingBannerSizesPortrait", []);
    };

    AATKit.fittingBannerSizesLandscape = function(resultCallback){
        cordova.exec(resultCallback, null, "AATKitCordovaPlugin", "fittingBannerSizesLandscape", []);
    };