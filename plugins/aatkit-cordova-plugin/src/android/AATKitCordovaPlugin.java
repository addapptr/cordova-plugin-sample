package com.addaptr.cordovaplugin;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;

import com.intentsoftware.addapptr.AATKit;
import com.intentsoftware.addapptr.AATKitAdNetworkOptions;
import com.intentsoftware.addapptr.AATKitConfiguration;
import com.intentsoftware.addapptr.AATKitDebugScreenConfiguration;
import com.intentsoftware.addapptr.AATKitHelper;
import com.intentsoftware.addapptr.AATKitImpression;
import com.intentsoftware.addapptr.AATKitRuntimeConfiguration;
import com.intentsoftware.addapptr.AdMobOptions;
import com.intentsoftware.addapptr.AdNetwork;
import com.intentsoftware.addapptr.AppNexusOptions;
import com.intentsoftware.addapptr.AppOpenAdPlacement;
import com.intentsoftware.addapptr.BannerPlacementSize;
import com.intentsoftware.addapptr.BannerSize;
import com.intentsoftware.addapptr.CollapsibleBannerOptions;
import com.intentsoftware.addapptr.Consent;
import com.intentsoftware.addapptr.DFPOptions;
import com.intentsoftware.addapptr.FeedAdOptions;
import com.intentsoftware.addapptr.FullscreenPlacement;
import com.intentsoftware.addapptr.ImpressionListener;
import com.intentsoftware.addapptr.ManagedConsent;
import com.intentsoftware.addapptr.NonIABConsent;
import com.intentsoftware.addapptr.Placement;
import com.intentsoftware.addapptr.PubNativeOptions;
import com.intentsoftware.addapptr.RewardedVideoPlacement;
import com.intentsoftware.addapptr.SimpleConsent;
import com.intentsoftware.addapptr.StickyBannerPlacement;
import com.intentsoftware.addapptr.VendorConsent;
import com.intentsoftware.addapptr.consent.CMP;
import com.intentsoftware.addapptr.consent.CMPGoogle;
import com.intentsoftware.addapptr.consent.CMPOgury;
import com.intentsoftware.addapptr.consent.CMPSourcePoint;
import com.intentsoftware.addapptr.internal.AATKitAbstractConfiguration;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AATKitCordovaPlugin extends CordovaPlugin {

    private static final String VERSION = "2.4.0";
    private static final String TAG = "AATKitCordova";

    private final static String AATKIT_CONFIGURATION_INITIALRULES = "initialRules";
    private final static String AATKIT_CONFIGURATION_SHOULDCACHERULES = "shouldCacheRules";
    private final static String AATKIT_CONFIGURATION_ALTERNATIVEBUNDLEID = "alternativeBundleId";
    private final static String AATKIT_CONFIGURATION_SHOULDREPORTUSINGALTERNATIVEBUNDLEID = "shouldReportUsingAlternativeBundleId";
    private final static String AATKIT_CONFIGURATION_TESTMODEACCOUNTID = "testModeAccountId";
    private final static String AATKIT_CONFIGURATION_USEDEBUGSHAKE = "useDebugShake";
    private final static String AATKIT_CONFIGURATION_USEGEOLOCATION = "useGeoLocation";
    private final static String AATKIT_CONFIGURATION_CONSENT = "consent";
    private final static String AATKIT_CONFIGURATION_CONSENTREQUIRED = "consentRequired";
    private final static String AATKIT_CONFIGURATION_SHOULDSKIPRULES= "shouldSkipRules";
    private final static String AATKIT_CONFIGURATION_PLATFORM= "platform";
    private final static String AATKIT_CONFIGURATION_SETISCHILDDIRECTED = "setIsChildDirected";

    private final static String AATKIT_CONSENT_TYPE = "type";
    private final static String AATKIT_CONSENT_TYPE_SIMPLECONSENT = "simpleconsent";
    private final static String AATKit_CONSENT_TYPE_MANAGED_CMP_GOOGLE = "managedcmpgoogle";
    private final static String AATKit_CONSENT_TYPE_MANAGED_CMP_OGURY = "managedcmpogury";
    private final static String AATKit_CONSENT_TYPE_MANAGED_CMP_SOURCE_POINT = "managedcmpsourcepoint";
    private final static String AATKIT_CONSENT_TYPE_VENDOR = "vendor";

    private final static String AATKIT_PLATFORM_ANDROID = "android";
    private final static String AATKIT_PLATFORM_HUAWEI = "huawei";

    private final static String AATKIT_CONSENT_NONIAB = "nonIABConsent";
    private final static String AATKIT_CONSENT_NONIAB_UNKNOWN = "unknown";
    private final static String AATKIT_CONSENT_NONIAB_OBTAINED = "obtained";
    private final static String AATKIT_CONSENT_NONIAB_WITHHELD = "withheld";

    // base
    private final static String AATKIT_INIT_WITH_CONFIGURATION = "initWithConfiguration";
    private final static String AATKIT_RECONFIGURE = "reconfigure";
    private final static String AATKIT_GET_VERSION = "getVersion";
    private final static String AATKIT_EDIT_CONSENT = "editConsent";
    private final static String AATKIT_RELOAD_CONSENT = "reloadConsent";
    private final static String AATKIT_SHOW_CONSENT_DIALOG_IF_NEEDED = "showConsentDialogIfNeeded";
    private final static String AATKIT_SET_DEBUG_ENABLED = "setDebugEnabled";
    private final static String AATKIT_SET_DEBUG_SHAKE_ENABLED = "setDebugShakeEnabled";
    private final static String AATKIT_GET_DEBUG_INFO = "getDebugInfo";
    private final static String AATKIT_CONFIGURE_DEBUG_SCREEN = "configureDebugScreen";

    //networks
    private final static String AATKIT_IS_NETWORK_ENABLED = "isNetworkEnabled";
    private final static String AATKIT_SET_NETWORK_ENABLED = "setNetworkEnabled";

    //placements
    private final static String AATKIT_CREATE_PLACEMENT = "createPlacement";
    private final static String AATKIT_CREATE_REWARDED_PLACEMENT = "createRewardedVideoPlacement";
    public static final String CREATE_APP_OPEN_AD_PLACEMENT = "createAppOpenAdPlacement";
    private final static String AATKIT_RELOAD_PLACEMENT = "reloadPlacement";
    private final static String AATKIT_RELOAD_PLACEMENT_FORCED = "reloadPlacementForced";
    private final static String AATKIT_START_PLACEMENT_AUTO_RELOAD = "startPlacementAutoReload";
    private final static String AATKIT_STOP_PLACEMENT_AUTO_RELOAD = "stopPlacementAutoReload";
    private final static String AATKIT_HAS_AD_FOR_PLACEMENT = "hasAdForPlacement";
    private final static String AATKIT_IS_FREQUENCY_CAP_REACHED_FOR_PLACEMENT = "isFrequencyCapReachedForPlacement";
    private final static String AATKIT_SET_IMPRESSION_LISTENER = "setImpressionListener";
    private final static String AATKIT_SET_COLLAPSIBLE_BANNER_OPTIONS = "setCollapsibleBannerOptions";

    //banners
    private final static String AATKIT_ADD_PLACEMENT_TO_VIEW = "addPlacementToView";
    private final static String AATKIT_REMOVE_PLACEMENT_FROM_VIEW = "removePlacementFromView";
    private final static String AATKIT_SET_PLACEMENT_ALIGNMENT = "setPlacementAlignment";
    private final static String AATKIT_SET_PLACEMENT_ALIGNMENT_WITH_OFFSET = "setPlacementAlignmentWithOffset";
    private final static String AATKIT_SET_PLACEMENT_POSITION = "setPlacementPosition";
    private final static String AATKIT_SET_PLACEMENT_CONTENT_GRAVITY = "setPlacementContentGravity";

    //fullscreens
    private final static String AATKIT_SHOW_PLACEMENT = "showPlacement";

    // targeting
    private final static String AATKIT_ADD_AD_NETWORK_FOR_KEYWORD_TARGETING = "addAdNetworkForKeywordTargeting";
    private final static String AATKIT_REMOVE_AD_NETWORK_FOR_KEYWORD_TARGETING = "removeAdNetworkForKeywordTargeting";
    public static final String SET_TARGETING_INFO = "setTargetingInfo";
    public static final String SET_TARGETING_INFO_FOR_PLACEMENT = "setTargetingInfoForPlacement";
    public static final String SET_CONTENT_TARGETING_URL = "setContentTargetingUrl";
    public static final String SET_CONTENT_TARGETING_URL_FOR_PLACEMENT = "setContentTargetingUrlForPlacement";
    public static final String AATKIT_CONSENT_ASSET_KEY_CMP_OGURY = "assetKeyCMPOgury";
    public static final String AATKIT_CONSENT_YOUR_ACCOUNT_ID = "yourAccountID";
    public static final String AATKIT_CONSENT_YOUR_PROPERTY_ID = "yourPropertyId";
    public static final String AATKIT_CONSENT_YOUR_PROPERTY_NAME = "yourPropertyName";
    public static final String AATKIT_CONSENT_YOUR_PM_ID = "yourPMId";
    public static final String AATKIT_VENDOR_CONSENT_FOR_ADDAPPTR = "consentForAddapptr";
    public static final String AATKIT_VENDOR_CONSENT_OBTAINED_NETWORKS = "vendorConsentObtainedNetworks";
    public static final String AATKIT_NO_CONSENT_NETWORK_STOP_SET = "noConsentNetworkStopSet";
    public static final String MUTE_VIDEO_ADS = "muteVideoAds";
    public static final String AATKIT_SET_PUBLISHER_PROVIDED_ID = "setPublisherProvidedId";
    public static final String AATKIT_IS_CONSENT_OPT_IN = "isConsentOptIn";
    public static final String AATKIT_SHOW_IF_NEEDED_SETTING = "showIfNeededSetting";
    public static final String AATKIT_SHOW_IF_NEEDED_SETTING_ALWAYS = "always";
    public static final String AATKIT_SHOW_IF_NEEDED_SETTING_NEVER = "never";
    public static final String AATKIT_SHOW_IF_NEEDED_SETTING_SERVER = "serverSideControl";

    // screen sizes
    public static final String IS_TABLET = "isTablet";
    public static final String MAXIMUM_BANNER_SIZE_PORTRAIT = "maximumBannerSizePortrait";
    public static final String MAXIMUM_BANNER_SIZE_LANDSCAPE = "maximumBannerSizeLandscape";
    public static final String FITTING_BANNER_SIZES_PORTRAIT = "fittingBannerSizesPortrait";
    public static final String FITTING_BANNER_SIZES_LANDSCAPE = "fittingBannerSizesLandscape";

    private ManagedConsent managedConsentNeedingUI;
    private ManagedConsent managedConsent;
    private Consent consent;

    private NonIABConsent consentForAddapptr;
    private Set<AdNetwork> vendorConsentObtainedNetworks = new HashSet<>();
    private static Map<String, StickyBannerPlacement> stickyBannerPlacements = new HashMap<>();
    private static Map<String, FullscreenPlacement> fullscreenPlacements = new HashMap<>();
    private static Map<String, RewardedVideoPlacement> rewardedVideoPlacements = new HashMap<>();
    private static Map<String, AppOpenAdPlacement> appOpenPlacements = new HashMap<>();
    private static Map<String, Boolean> placementsAutoreload = new HashMap<>();

    private static boolean debugLog = false;

    @Override
    protected void pluginInitialize() {
        super.pluginInitialize();
        AATKitHelper.setPluginVersion(VERSION);

    }

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callback) throws JSONException {

        switch (action) {
            case AATKIT_INIT_WITH_CONFIGURATION:
                initWithConfiguration(args);
                return true;
            case AATKIT_RECONFIGURE:
                reconfigure(args);
                return true;
            case AATKIT_GET_VERSION:
                getVersion(callback);
                return true;
            case AATKIT_EDIT_CONSENT:
                editConsent();
                return true;
            case AATKIT_SHOW_CONSENT_DIALOG_IF_NEEDED:
                showConsentDialogIfNeeded();
                return true;
            case AATKIT_RELOAD_CONSENT:
                reloadConsent();
                return true;
            case AATKIT_SET_DEBUG_ENABLED:
                setDebugEnabled(args);
                return true;
            case AATKIT_SET_DEBUG_SHAKE_ENABLED:
                setDebugShakeEnabled(args);
                return true;
            case AATKIT_GET_DEBUG_INFO:
                getDebugInfo(callback);
                return true;
            case AATKIT_CONFIGURE_DEBUG_SCREEN:
                configureDebugScreen(args);
                return true;
            case AATKIT_CONFIGURATION_SETISCHILDDIRECTED:
                setIsChildDirected(args);
                return true;
            case AATKIT_IS_CONSENT_OPT_IN:
                isConsentOptIn(callback);
                return true;

            case AATKIT_SET_NETWORK_ENABLED:
                setNetworkEnabled(args);
                return true;
            case AATKIT_IS_NETWORK_ENABLED:
                isNetworkEnabled(args, callback);
                return true;

            case AATKIT_CREATE_PLACEMENT:
                createPlacement(args);
                return true;
            case AATKIT_CREATE_REWARDED_PLACEMENT:
                createRewardedVideoPlacement(args);
                return true;
            case CREATE_APP_OPEN_AD_PLACEMENT:
                createAppOpenAdPlacement(args);
                return true;
            case AATKIT_RELOAD_PLACEMENT:
                reloadPlacement(args, callback);
                return true;
            case AATKIT_RELOAD_PLACEMENT_FORCED:
                reloadPlacementForced(args, callback);
                return true;
            case AATKIT_START_PLACEMENT_AUTO_RELOAD:
                startPlacementAutoReload(args);
                return true;
            case AATKIT_STOP_PLACEMENT_AUTO_RELOAD:
                stopPlacementAutoReload(args);
                return true;
            case AATKIT_HAS_AD_FOR_PLACEMENT:
                hasAdForPlacement(args, callback);
                return true;
            case AATKIT_IS_FREQUENCY_CAP_REACHED_FOR_PLACEMENT:
                isFrequencyCapReachedForPlacement(args, callback);
                return true;
            case MUTE_VIDEO_ADS:
                muteVideoAds(args);
                return true;
            case AATKIT_SET_IMPRESSION_LISTENER:
                setImpressionListener(args);
                return true;
            case AATKIT_SET_PUBLISHER_PROVIDED_ID:
                setPublisherProvidedId(args);
                return true;
            case AATKIT_SET_COLLAPSIBLE_BANNER_OPTIONS:
                setCollapsibleBannerOptions(args);
                return true;

            case AATKIT_ADD_PLACEMENT_TO_VIEW:
                addPlacementToView(args);
                return true;
            case AATKIT_REMOVE_PLACEMENT_FROM_VIEW:
                removePlacementFromView(args);
                return true;
            case AATKIT_SET_PLACEMENT_ALIGNMENT:
                setPlacementAlignment(args);
                return true;
            case AATKIT_SET_PLACEMENT_ALIGNMENT_WITH_OFFSET:
                setPlacementAlignmentWithOffset(args);
                return true;
            case AATKIT_SET_PLACEMENT_POSITION:
                setPlacementPosition(args);
                return true;
            case AATKIT_SET_PLACEMENT_CONTENT_GRAVITY:
                setPlacementContentGravity(args);
                return true;

            case AATKIT_SHOW_PLACEMENT:
                showPlacement(args, callback);
                return true;

            case SET_TARGETING_INFO:
                setTargetingInfo(args);
                return true;
            case SET_TARGETING_INFO_FOR_PLACEMENT:
                setTargetingInfoForPlacement(args);
                return true;
            case SET_CONTENT_TARGETING_URL:
                setContentTargetingUrl(args);
                return true;
            case SET_CONTENT_TARGETING_URL_FOR_PLACEMENT:
                setContentTargetingUrlForPlacement(args);
                return true;
            case AATKIT_ADD_AD_NETWORK_FOR_KEYWORD_TARGETING:
                addAdNetworkForKeywordTargeting(args);
                return true;
            case AATKIT_REMOVE_AD_NETWORK_FOR_KEYWORD_TARGETING:
                removeAdNetworkForKeywordTargeting(args);
                return true;

            case IS_TABLET:
                isTablet(callback);
                return true;
            case MAXIMUM_BANNER_SIZE_PORTRAIT:
                maximumBannerSizePortrait(callback);
                return true;
            case MAXIMUM_BANNER_SIZE_LANDSCAPE:
                maximumBannerSizeLandscape(callback);
                return true;
            case FITTING_BANNER_SIZES_PORTRAIT:
                fittingBannerSizesPortrait(callback);
                return true;
            case FITTING_BANNER_SIZES_LANDSCAPE:
                fittingBannerSizesLandscape(callback);
                return true;

            default:
                return false;
        }
    }

    // region AATKit methods

    // region AATKit methods - base

    private void initWithConfiguration(JSONArray args) throws JSONException {

        JSONObject jsonObject = args.getJSONObject(0);
        log("initWithConfiguration: " + args);

        AATKitConfiguration aatKitConfiguration = new AATKitConfiguration(cordova.getActivity().getApplication());

        aatKitConfiguration.setDelegate(createDelegate());

        if (jsonObject.has(AATKIT_CONFIGURATION_INITIALRULES))
            aatKitConfiguration.setInitialRules(jsonObject.getString(AATKIT_CONFIGURATION_INITIALRULES));

        if (jsonObject.has(AATKIT_CONFIGURATION_SHOULDCACHERULES))
            aatKitConfiguration.setShouldCacheRules(jsonObject.getBoolean(AATKIT_CONFIGURATION_SHOULDCACHERULES));

        if (jsonObject.has(AATKIT_CONFIGURATION_ALTERNATIVEBUNDLEID))
            aatKitConfiguration.setAlternativeBundleId(jsonObject.getString(AATKIT_CONFIGURATION_ALTERNATIVEBUNDLEID));

        if (jsonObject.has(AATKIT_CONFIGURATION_SHOULDREPORTUSINGALTERNATIVEBUNDLEID))
            aatKitConfiguration.setShouldReportUsingAlternativeBundleId(jsonObject.getBoolean(AATKIT_CONFIGURATION_SHOULDREPORTUSINGALTERNATIVEBUNDLEID));

        if (jsonObject.has(AATKIT_CONFIGURATION_TESTMODEACCOUNTID))
            aatKitConfiguration.setTestModeAccountId(jsonObject.getInt(AATKIT_CONFIGURATION_TESTMODEACCOUNTID));

        if (jsonObject.has(AATKIT_CONFIGURATION_USEDEBUGSHAKE))
            aatKitConfiguration.setUseDebugShake(jsonObject.getBoolean(AATKIT_CONFIGURATION_USEDEBUGSHAKE));

        if(jsonObject.has(AATKIT_CONFIGURATION_SHOULDSKIPRULES))
            aatKitConfiguration.setShouldSkipRules(jsonObject.getBoolean(AATKIT_CONFIGURATION_SHOULDSKIPRULES));

        if (jsonObject.has(AATKIT_CONFIGURATION_PLATFORM)) {
            String platform = jsonObject.getString(AATKIT_CONFIGURATION_PLATFORM);
            if (platform.equalsIgnoreCase(AATKIT_PLATFORM_HUAWEI)) {
                aatKitConfiguration.setPlatform(AATKitConfiguration.Platform.HUAWEI);
            } else if (platform.equalsIgnoreCase(AATKIT_PLATFORM_ANDROID)) {
                aatKitConfiguration.setPlatform(AATKitConfiguration.Platform.ANDROID);
            }
        }

        setRuntimeConfiguration(aatKitConfiguration, jsonObject);
        setNetworkOptions(aatKitConfiguration, jsonObject);

        cordova.getActivity().runOnUiThread(() -> {

            if (!AATKitHelper.isInitialized()) {
                AATKit.init(aatKitConfiguration);
                AATKit.onActivityResume(cordova.getActivity());
            }
        });
    }

    private void setNetworkOptions(AATKitConfiguration configuration, JSONObject initConfig) throws JSONException {
        if(!initConfig.has("networkOptions")) {
            return;
        }
        final JSONObject options = initConfig.getJSONObject("networkOptions");
        final AppNexusOptions appNexusOptions = getAppNexusOptions(options);
        final FeedAdOptions feedAdOptions = getFeedAdOptions(options);
        final PubNativeOptions pubNativeOptions = getPubNativeOptions(options);
        final AdMobOptions adMobOptions = getAdMobOptions(options);
        final DFPOptions dfpOptions = getDfpOptions(options);
        final AATKitAdNetworkOptions networkOptions = new AATKitAdNetworkOptions();
        networkOptions.setAppNexusOptions(appNexusOptions);
        networkOptions.setFeedAdOptions(feedAdOptions);
        networkOptions.setPubNativeOptions(pubNativeOptions);
        networkOptions.setAdMobOptions(adMobOptions);
        networkOptions.setDfpOptions(dfpOptions);
        configuration.setAdNetworkOptions(networkOptions);
    }

    private AppNexusOptions getAppNexusOptions(JSONObject options) throws JSONException {
        if (!options.has("appNexusOptions")) {
            return null;
        }
        JSONObject networkOptions = options.getJSONObject("appNexusOptions");
        Integer autoCloseTime = networkOptions.has("autoCloseTime") ? networkOptions.getInt("autoCloseTime") : null;
        Boolean supportNativeBanner = networkOptions.has("supportNativeBanner") ? networkOptions.getBoolean("supportNativeBanner") : null;
        Boolean supportVideoBanner = networkOptions.has("supportVideoBanner") ? networkOptions.getBoolean("supportVideoBanner") : null;
        final AppNexusOptions actualOptions = new AppNexusOptions();
        if(autoCloseTime != null) {
            actualOptions.setAutoCloseTime(autoCloseTime);
        }
        if(supportNativeBanner != null) {
            actualOptions.setSupportNativeBanner(supportNativeBanner);
        }
        if(supportVideoBanner != null) {
            actualOptions.setSupportVideoBanner(supportVideoBanner);
        }
        return actualOptions;
    }

    private FeedAdOptions getFeedAdOptions(JSONObject options) throws JSONException {
        if (!options.has("feedAdOptions")) {
            return null;
        }
        JSONObject feedAdOptions = options.getJSONObject("feedAdOptions");
        Integer shutterColor = feedAdOptions.has("shutterColor") ? feedAdOptions.getInt("shutterColor") : null;
        Boolean disableSpinner = feedAdOptions.has("disableSpinner") ? feedAdOptions.getBoolean("disableSpinner") : null;
        final FeedAdOptions actualOptions = new FeedAdOptions();
        if(shutterColor != null) {
            actualOptions.setShutterColor(shutterColor);
        }
        if(disableSpinner != null) {
            actualOptions.setDisableSpinner(disableSpinner);
        }
        return actualOptions;
    }

    private PubNativeOptions getPubNativeOptions(JSONObject options) throws JSONException {
        if (!options.has("pubNativeOptions")) {
            return null;
        }
        JSONObject pubNativeOptions = options.getJSONObject("pubNativeOptions");
        Integer skipOffsetForHTMLInterstitial = pubNativeOptions.has("skipOffsetForHTMLInterstitial") ? pubNativeOptions.getInt("skipOffsetForHTMLInterstitial") : null;
        Integer skipOffsetForVideoInterstitial = pubNativeOptions.has("skipOffsetForVideoInterstitial") ? pubNativeOptions.getInt("skipOffsetForVideoInterstitial") : null;
        final PubNativeOptions actualOptions = new PubNativeOptions();
        if(skipOffsetForHTMLInterstitial != null) {
            actualOptions.setSkipOffsetForHTMLInterstitial(skipOffsetForHTMLInterstitial);
        }
        if(skipOffsetForVideoInterstitial != null) {
            actualOptions.setSkipOffsetForVideoInterstitial(skipOffsetForVideoInterstitial);
        }
        return actualOptions;
    }

    private AdMobOptions getAdMobOptions(JSONObject options) throws JSONException {
        if (!options.has("adMobOptions")) {
            return null;
        }
        JSONObject adMobOptions = options.getJSONObject("adMobOptions");
        Integer inlineBannerMaxHeight = adMobOptions.has("inlineBannerMaxHeight") ? adMobOptions.getInt("inlineBannerMaxHeight") : null;
        if(inlineBannerMaxHeight == null) {
            return null;
        }
        return new AdMobOptions(inlineBannerMaxHeight);
    }

    private DFPOptions getDfpOptions(JSONObject options) throws JSONException {
        if (!options.has("dfpOptions")) {
            return null;
        }
        JSONObject dfpOptions = options.getJSONObject("dfpOptions");
        Integer inlineBannerMaxHeight = dfpOptions.has("inlineBannerMaxHeight") ? dfpOptions.getInt("inlineBannerMaxHeight") : null;
        if(inlineBannerMaxHeight == null) {
            return null;
        }
        return new DFPOptions(inlineBannerMaxHeight);
    }

    private void reconfigure(JSONArray args) throws JSONException {
        JSONObject jsonObject = args.getJSONObject(0);
        log("reconfigure: " + args);

        AATKitRuntimeConfiguration aatKitRuntimeConfiguration = new AATKitRuntimeConfiguration();

        setRuntimeConfiguration(aatKitRuntimeConfiguration, jsonObject);

        cordova.getActivity().runOnUiThread(() -> {
            AATKit.reconfigure(aatKitRuntimeConfiguration);
        });
    }

    private void getVersion(CallbackContext callback) {
        log("getVersion");

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, AATKit.getVersion()));
            }
        });
    }

    private void editConsent() {
        log("editConsent");

        cordova.getActivity().runOnUiThread(() -> {
            if (managedConsent != null) {
                managedConsent.editConsent(cordova.getActivity());
            }
        });
    }

    private void showConsentDialogIfNeeded() {
        log("showConsentDialogIfNeeded");

        cordova.getActivity().runOnUiThread(() -> {
            Activity activity = cordova.getActivity();

            if (managedConsentNeedingUI != null) {
                managedConsentNeedingUI.showIfNeeded(activity);
                managedConsentNeedingUI = null;
            } else if(managedConsent != null) {
                managedConsent.showIfNeeded(activity);
            }
        });
    }

    private void reloadConsent() {
        log("reloadConsent");

        cordova.getActivity().runOnUiThread(() -> {
            if (managedConsent != null) {
                managedConsent.reload(cordova.getActivity());
            }
        });
    }

    private void setDebugEnabled(JSONArray args) throws JSONException {

        boolean enabled = args.getBoolean(0);

        log(true, "setDebugEnabled: " + enabled);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                if (enabled)
                    AATKit.setLogLevel(Log.VERBOSE);
                else
                    AATKit.setLogLevel(Log.INFO);
            }
            debugLog = enabled;
        });
    }

    private void setDebugShakeEnabled(JSONArray args) throws JSONException {

        boolean enabled = args.getBoolean(0);

        log("setDebugShakeEnabled: " + enabled);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                if (enabled)
                    AATKit.enableDebugScreen();
                else
                    AATKit.disableDebugScreen();
            }
        });
    }

    private void getDebugInfo(CallbackContext callback) {

        log("getDebugInfo");

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, AATKit.getDebugInfo()));
            }
        });
    }

    private void configureDebugScreen(JSONArray args) throws JSONException {
        JSONObject configuration = args.getJSONObject(0);
        log("configureDebugScreen: " + configuration);
        cordova.getActivity().runOnUiThread(() -> {
            try {
                String title = configuration.getString("title");
                boolean showBundleId = configuration.has("showBundleId") ? configuration.getBoolean("showBundleId") : true;
                boolean showTestMode = configuration.has("showTestMode") ? configuration.getBoolean("showTestMode") : true;
                boolean showLoadedAndLoadingAds = configuration.has("showLoadedAndLoadingAds") ? configuration.getBoolean("showLoadedAndLoadingAds") : true;
                boolean showAvailableNetworks = configuration.has("showAvailableNetworks") ? configuration.getBoolean("showAvailableNetworks") : true;
                boolean showDisabledNetworks = configuration.has("showDisabledNetworks") ? configuration.getBoolean("showDisabledNetworks") : true;
                boolean showRemovedNetworkSDKs = configuration.has("showRemovedNetworkSDKs") ? configuration.getBoolean("showRemovedNetworkSDKs") : true;
                boolean showUnsupportedNetworks = configuration.has("showUnsupportedNetworks") ? configuration.getBoolean("showUnsupportedNetworks") : true;
                boolean showExtraSDKs = configuration.has("showExtraSDKs") ? configuration.getBoolean("showExtraSDKs") : true;
                boolean showConsent = configuration.has("showConsent") ? configuration.getBoolean("showConsent") : true;
                boolean showAdvertisingId = configuration.has("showAdvertisingId") ? configuration.getBoolean("showAdvertisingId") : true;
                boolean showDeviceType = configuration.has("showDeviceType") ? configuration.getBoolean("showDeviceType") : true;
                Drawable icon = getAppIcon();
                final AATKitDebugScreenConfiguration debugScreenConfiguration = new AATKitDebugScreenConfiguration(icon, title);
                debugScreenConfiguration.setShowBundleId(showBundleId);
                debugScreenConfiguration.setShowTestMode(showTestMode);
                debugScreenConfiguration.setShowLoadedAndLoadingAds(showLoadedAndLoadingAds);
                debugScreenConfiguration.setShowAvailableNetworks(showAvailableNetworks);
                debugScreenConfiguration.setShowDisabledNetworks(showDisabledNetworks);
                debugScreenConfiguration.setShowRemovedNetworkSDKs(showRemovedNetworkSDKs);
                debugScreenConfiguration.setShowUnsupportedNetworks(showUnsupportedNetworks);
                debugScreenConfiguration.setShowExtraSDKs(showExtraSDKs);
                debugScreenConfiguration.setShowConsent(showConsent);
                debugScreenConfiguration.setShowAdvertisingId(showAdvertisingId);
                debugScreenConfiguration.setShowDeviceType(showDeviceType);
                AATKit.configureDebugScreen(debugScreenConfiguration);
            } catch (JSONException e) {
                logWarn(e.getMessage());
            }
        });
    }

    private Drawable getAppIcon() {
        Drawable icon;
        final String packageName = cordova.getContext().getPackageName();
        try {
            icon = cordova.getContext().getPackageManager().getApplicationIcon(packageName);
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(e);
        }
        return icon;
    }

    private void setIsChildDirected(JSONArray args) throws JSONException {
        boolean enabled = args.getBoolean(0);

        log("setIsChildDirected: " + enabled);

        cordova.getActivity().runOnUiThread(() -> {
            AATKit.setIsChildDirected(enabled);
        });
    }

    // endregion AATKit methods - base

    // region AATKit methods - networks

    private void setNetworkEnabled(JSONArray args) throws JSONException {

        String networkName = args.getString(0);
        boolean enabled = args.getBoolean(1);

        log("setNetworkEnabled: networkName: " + networkName + ", enabled: " + enabled);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {

                AdNetwork adNetwork = AATKitHelper.getAdNetworkFromName(networkName);

                if (adNetwork != null) {
                    AATKit.setNetworkEnabled(adNetwork, enabled);
                } else {
                    log(networkName + " is not supported on this platform.");
                }
            }
        });
    }

    private void isNetworkEnabled(JSONArray args, CallbackContext callback) throws JSONException {

        String networkName = args.getString(0);

        log("isNetworkEnabled: networkName: " + networkName);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {

                AdNetwork adNetwork = AATKitHelper.getAdNetworkFromName(networkName);

                if (adNetwork != null) {
                    callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, AATKit.isNetworkEnabled(adNetwork)));
                } else {
                    log(networkName + " is not supported on this platform.");
                }
            }
        });
    }

    // endregion AATKit methods - networks

    // region AATKit methods - placements

    private void createPlacement(JSONArray args) throws JSONException {

        String placementName = args.getString(0);
        String placementSize = args.getString(1);

        log("createPlacement: placementName: " + placementName + ", placementSize: " + placementSize);

        cordova.getActivity().runOnUiThread(() -> {
            switch (placementSize) {
                case "Banner320x53":
                    createStickyBannerPlacement(placementName, BannerPlacementSize.Banner320x53);
                    break;
                case "Banner320x50":
                    createStickyBannerPlacement(placementName, BannerPlacementSize.Banner320x50);
                    break;
                case "Banner768x90":
                    createStickyBannerPlacement(placementName, BannerPlacementSize.Banner768x90);
                    break;
                case "Banner300x250":
                    createStickyBannerPlacement(placementName, BannerPlacementSize.Banner300x250);
                    break;
                case "Banner468x60":
                    createStickyBannerPlacement(placementName, BannerPlacementSize.Banner468x60);
                    break;
                case "Fullscreen":
                    createFullscreenPlacement(placementName);
                    break;
            }
        });
    }

    private void createStickyBannerPlacement(String placementName, BannerPlacementSize size) {
        AATKit.StatisticsListener statisticsListener = createStatisticsListener(placementName);
        StickyBannerPlacement placement = AATKit.createStickyBannerPlacement(placementName, size);
        placement.setListener(new AATKitPlacementListener(placementName, webView));
        placement.setStatisticsListener(statisticsListener);
        stickyBannerPlacements.put(placementName, placement);
    }

    private void createFullscreenPlacement(String placementName) {
        AATKit.StatisticsListener statisticsListener = createStatisticsListener(placementName);
        FullscreenPlacement placement = AATKit.createFullscreenPlacement(placementName);
        placement.setListener(new AATKitPlacementListener(placementName, webView));
        placement.setStatisticsListener(statisticsListener);
        fullscreenPlacements.put(placementName, placement);
    }

    private void createRewardedVideoPlacement(JSONArray args) throws JSONException {
        String placementName = args.getString(0);
        log("createPlacement: placementName: " + placementName);

        cordova.getActivity().runOnUiThread(() -> {
            AATKit.StatisticsListener statisticsListener = createStatisticsListener(placementName);
            RewardedVideoPlacement placement = AATKit.createRewardedVideoPlacement(placementName);
            placement.setListener(new AATKitPlacementListener(placementName, webView));
            placement.setStatisticsListener(statisticsListener);
            rewardedVideoPlacements.put(placementName, placement);
        });
    }

    private void createAppOpenAdPlacement(JSONArray args) throws JSONException {

        String placementName = args.getString(0);

        log("createAppOpenAdPlacement: placementName: " + placementName);

        cordova.getActivity().runOnUiThread(() -> {
            AATKit.StatisticsListener statisticsListener = createStatisticsListener(placementName);
            AppOpenAdPlacement placement = AATKit.createAppOpenAdPlacement(placementName);
            placement.setListener(new AATKitPlacementListener(placementName, webView));
            placement.setStatisticsListener(statisticsListener);
            appOpenPlacements.put(placementName, placement);
        });
    }

    private void reloadPlacement(JSONArray args, CallbackContext callback) throws JSONException {

        String placementName = args.getString(0);

        log("reloadPlacement: placementName: " + placementName);

        cordova.getActivity().runOnUiThread(() -> {
            boolean reloadResult = false;

            if(stickyBannerPlacements.containsKey(placementName)) {
                StickyBannerPlacement placement = stickyBannerPlacements.get(placementName);
                reloadResult = placement.reload();
            } else if(fullscreenPlacements.containsKey(placementName)) {
                FullscreenPlacement placement = fullscreenPlacements.get(placementName);
                reloadResult = placement.reload();
            } else if(rewardedVideoPlacements.containsKey(placementName)) {
                RewardedVideoPlacement placement = rewardedVideoPlacements.get(placementName);
                reloadResult = placement.reload();
            }  else if(appOpenPlacements.containsKey(placementName)) {
                AppOpenAdPlacement placement = appOpenPlacements.get(placementName);
                reloadResult = placement.reload();
            } else {
                logWarn("Placement " + placementName + " does not exist.");
            }

            callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, reloadResult));
        });
    }

    private void reloadPlacementForced(JSONArray args, CallbackContext callback) throws JSONException {

        String placementName = args.getString(0);
        boolean forced = args.getBoolean(1);

        log("reloadPlacementForced: placementName: " + placementName + ", forced: " + forced);

        cordova.getActivity().runOnUiThread(() -> {
            boolean reloadResult = false;

            if(stickyBannerPlacements.containsKey(placementName)) {
                StickyBannerPlacement placement = stickyBannerPlacements.get(placementName);
                reloadResult = placement.reload(forced);
            } else {
                logWarn("Placement " + placementName + " does not exist.");
            }

            callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, reloadResult));
        });
    }

    private void startPlacementAutoReload(JSONArray args) throws JSONException {

        String placementName = args.getString(0);

        log("startPlacementAutoReload: placementName: " + placementName);

        cordova.getActivity().runOnUiThread(() -> {
            if(stickyBannerPlacements.containsKey(placementName)) {
                StickyBannerPlacement placement = stickyBannerPlacements.get(placementName);
                placement.startAutoReload();
                placementsAutoreload.put(placementName, true);
            } else if(fullscreenPlacements.containsKey(placementName)) {
                FullscreenPlacement placement = fullscreenPlacements.get(placementName);
                placement.startAutoReload();
                placementsAutoreload.put(placementName, true);
            } else if(rewardedVideoPlacements.containsKey(placementName)) {
                RewardedVideoPlacement placement = rewardedVideoPlacements.get(placementName);
                placement.startAutoReload();
                placementsAutoreload.put(placementName, true);
            }  else if(appOpenPlacements.containsKey(placementName)) {
                AppOpenAdPlacement placement = appOpenPlacements.get(placementName);
                placement.startAutoReload();
                placementsAutoreload.put(placementName, true);
            } else {
                logWarn("Placement " + placementName + " does not exist.");
            }
        });
    }

    private void stopPlacementAutoReload(JSONArray args) throws JSONException {

        String placementName = args.getString(0);

        log("stopPlacementAutoReload: placementName: " + placementName);

        cordova.getActivity().runOnUiThread(() -> {
            if(stickyBannerPlacements.containsKey(placementName)) {
                StickyBannerPlacement placement = stickyBannerPlacements.get(placementName);
                placement.stopAutoReload();
                placementsAutoreload.put(placementName, false);
            } else if(fullscreenPlacements.containsKey(placementName)) {
                FullscreenPlacement placement = fullscreenPlacements.get(placementName);
                placement.stopAutoReload();
                placementsAutoreload.put(placementName, false);
            } else if(rewardedVideoPlacements.containsKey(placementName)) {
                RewardedVideoPlacement placement = rewardedVideoPlacements.get(placementName);
                placement.stopAutoReload();
                placementsAutoreload.put(placementName, false);
            }  else if(appOpenPlacements.containsKey(placementName)) {
                AppOpenAdPlacement placement = appOpenPlacements.get(placementName);
                placement.stopAutoReload();
                placementsAutoreload.put(placementName, false);
            } else {
                logWarn("Placement " + placementName + " does not exist.");
            }
        });
    }

    private void hasAdForPlacement(JSONArray args, CallbackContext callback) throws JSONException {

        String placementName = args.getString(0);

        log("hasAdForPlacement: placementName: " + placementName);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                boolean hasAd = false;

                if(stickyBannerPlacements.containsKey(placementName)) {
                    StickyBannerPlacement placement = stickyBannerPlacements.get(placementName);
                    hasAd = placement.hasAd();
                } else if(fullscreenPlacements.containsKey(placementName)) {
                    FullscreenPlacement placement = fullscreenPlacements.get(placementName);
                    hasAd = placement.hasAd();
                } else if(rewardedVideoPlacements.containsKey(placementName)) {
                    RewardedVideoPlacement placement = rewardedVideoPlacements.get(placementName);
                    hasAd = placement.hasAd();
                }  else if(appOpenPlacements.containsKey(placementName)) {
                    AppOpenAdPlacement placement = appOpenPlacements.get(placementName);
                    hasAd = placement.hasAd();
                }else {
                    log("Placement " + placementName + " does not exist.");
                }

                callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, hasAd));
            }
        });
    }

    private void setCollapsibleBannerOptions(JSONArray args) throws JSONException {
        String placementName = args.getString(0);
        JSONObject collapsibleBannerOptions = args.getJSONObject(1);

        log("setCollapsibleBannerOptions placementName: " + placementName + " collapsibleBannerOptions: " + collapsibleBannerOptions);
        cordova.getActivity().runOnUiThread(() -> {
            try {
                CollapsibleBannerOptions.CollapsiblePosition position = getCollapsiblePosition(collapsibleBannerOptions);
                Integer minDelayInSeconds = collapsibleBannerOptions.has("minDelayInSeconds") ? collapsibleBannerOptions.getInt("minDelayInSeconds") : null;
                final CollapsibleBannerOptions options = getCollapsibleBannerOptions(position, minDelayInSeconds);
                if (stickyBannerPlacements.containsKey(placementName)) {
                    StickyBannerPlacement bannerPlacement = stickyBannerPlacements.get(placementName);
                    bannerPlacement.setCollapsibleBannerOptions(options);
                } else {
                    logWarn("Placement " + placementName + " does not exist.");
                }
            } catch (JSONException e) {
                logWarn(e.getMessage());
            }
        });
    }

    private static CollapsibleBannerOptions.CollapsiblePosition getCollapsiblePosition(JSONObject collapsibleBannerOptions) throws JSONException {
        CollapsibleBannerOptions.CollapsiblePosition position;
        String positionRaw = collapsibleBannerOptions.has("position") ? collapsibleBannerOptions.getString("position") : "top";
        if (positionRaw.equalsIgnoreCase("bottom")) {
            position = CollapsibleBannerOptions.CollapsiblePosition.BOTTOM;
        } else {
            position = CollapsibleBannerOptions.CollapsiblePosition.TOP;
        }
        return position;
    }

    private static CollapsibleBannerOptions getCollapsibleBannerOptions(CollapsibleBannerOptions.CollapsiblePosition position, Integer minDelayInSeconds) {
        final CollapsibleBannerOptions options;
        if (minDelayInSeconds != null) {
            options = new CollapsibleBannerOptions(position, minDelayInSeconds);
        } else {
            options = new CollapsibleBannerOptions(position);
        }
        return options;
    }

    private void isFrequencyCapReachedForPlacement(JSONArray args, CallbackContext callback) throws JSONException {

        String placementName = args.getString(0);

        log("isFrequencyCapReachedForPlacement: placementName: " + placementName);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                boolean frequencyCapReached = false;

                if(fullscreenPlacements.containsKey(placementName)) {
                    FullscreenPlacement placement = fullscreenPlacements.get(placementName);
                    frequencyCapReached = placement.isFrequencyCapReached();
                } else if(rewardedVideoPlacements.containsKey(placementName)) {
                    RewardedVideoPlacement placement = rewardedVideoPlacements.get(placementName);
                    frequencyCapReached = placement.isFrequencyCapReached();
                }  else if(appOpenPlacements.containsKey(placementName)) {
                    AppOpenAdPlacement placement = appOpenPlacements.get(placementName);
                    frequencyCapReached = placement.isFrequencyCapReached();
                } else {
                    log("Placement " + placementName + " does not exist.");
                }

                callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, frequencyCapReached));
            }
        });
    }

    private void muteVideoAds(JSONArray args) throws JSONException {
        boolean mute = args.getBoolean(0);

        log("muteVideoAds mute: " + mute);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                AATKit.muteVideoAds(mute);
            }
        });
    }

    private void setImpressionListener(JSONArray args) throws JSONException {
        String placementName = args.getString(0);

        log("setImpressionListener placementName: " + placementName);

        cordova.getActivity().runOnUiThread(() -> {
            if(AATKitHelper.isInitialized()) {
                if(stickyBannerPlacements.containsKey(placementName)) {
                    StickyBannerPlacement placement = stickyBannerPlacements.get(placementName);
                    placement.setImpressionListener(createImpressionListener(placementName));
                } else if(fullscreenPlacements.containsKey(placementName)) {
                    FullscreenPlacement placement = fullscreenPlacements.get(placementName);
                    placement.setImpressionListener(createImpressionListener(placementName));
                } else if(rewardedVideoPlacements.containsKey(placementName)) {
                    RewardedVideoPlacement placement = rewardedVideoPlacements.get(placementName);
                    placement.setImpressionListener(createImpressionListener(placementName));
                }  else if(appOpenPlacements.containsKey(placementName)) {
                    AppOpenAdPlacement placement = appOpenPlacements.get(placementName);
                    placement.setImpressionListener(createImpressionListener(placementName));
                }
            }
        });
    }

    @NonNull
    private ImpressionListener createImpressionListener(String placementName) {
        return new ImpressionListener() {
            @Override
            public void didCountImpression(@NonNull Placement placement, @NonNull AATKitImpression impression) {
                String url = String.format(
                        "javascript:cordova.fireDocumentEvent('%s', { 'placementName': '%s', 'bannerSize': '%s', 'mediationTypeName': '%s', 'mediationType': '%s', 'adNetworkName': '%s', 'networkKey': '%s', 'adNetwork': '%s', 'price': '%s', 'currencyCode': '%s', 'precision': '%s'});",
                        "didCountImpression",
                        placementName,
                        impression.getBannerSize() != null ? impression.getBannerSize() : "",
                        impression.getMediationTypeName(),
                        impression.getMediationType(),
                        impression.getAdNetworkName(),
                        impression.getNetworkKey(),
                        AATKitHelper.getNameFromAdNetwork(impression.getAdNetwork()),
                        impression.getPrice(),
                        impression.getCurrencyCode() != null ? impression.getCurrencyCode() : "",
                        impression.getPrecision().name()
                );
                webView.loadUrl(url);
            }
        };
    }

    // endregion AATKIt methods - placements

    // region AATKit methods - banners

    private void addPlacementToView(JSONArray args) throws JSONException {

        String placementName = args.getString(0);

        log("addPlacementToView: placementName: " + placementName);

        cordova.getActivity().runOnUiThread(() -> {

            if (AATKitHelper.isInitialized() && stickyBannerPlacements.containsKey(placementName)) {
                StickyBannerPlacement stickyBannerPlacement = stickyBannerPlacements.get(placementName);
                View placementView = stickyBannerPlacement.getPlacementView();

                if (placementView.getParent() == null) {
                    FrameLayout mainLayout = (FrameLayout) cordova.getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
                    mainLayout.addView(placementView);
                }

                if (placementsAutoreload.containsKey(placementName) && placementsAutoreload.get(placementName)) {
                    stickyBannerPlacement.startAutoReload();
                }
            }
        });

    }

    private void removePlacementFromView(JSONArray args) throws JSONException {

        String placementName = args.getString(0);

        log("removePlacementFromView: placementName: " + placementName);

        cordova.getActivity().runOnUiThread(() -> {

            if (AATKitHelper.isInitialized() && stickyBannerPlacements.containsKey(placementName)) {
                StickyBannerPlacement bannerPlacement = stickyBannerPlacements.get(placementName);
                View placementView = bannerPlacement.getPlacementView();
                FrameLayout mainLayout = (FrameLayout) cordova.getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
                mainLayout.removeView(placementView);

                if (placementsAutoreload.containsKey(placementName) && placementsAutoreload.get(placementName)) {
                    bannerPlacement.stopAutoReload();
                }
            }
        });
    }

    private void setPlacementAlignment(JSONArray args) throws JSONException {

        String placementName = args.getString(0);
        String bannerAlignment = args.getString(1);

        log("setPlacementAlignment: placementName: " + placementName + ", bannerAlignment: " + bannerAlignment);

        FrameLayout.LayoutParams layoutParams = AATKitHelper.getLayoutParamsFromAlignment(bannerAlignment);

        cordova.getActivity().runOnUiThread(() -> {

            if (AATKitHelper.isInitialized() && stickyBannerPlacements.containsKey(placementName)) {
                StickyBannerPlacement bannerPlacement = stickyBannerPlacements.get(placementName);
                View placementView = bannerPlacement.getPlacementView();
                placementView.setLayoutParams(layoutParams);
            }
        });
    }

    private void setPlacementAlignmentWithOffset(JSONArray args) throws JSONException {

        String placementName = args.getString(0);
        String bannerAlignment = args.getString(1);
        int x = args.getInt(2);
        int y = args.getInt(3);

        log("setPlacementAlignmentWithOffset: placementName: " + placementName + ", bannerAlignment: " + bannerAlignment + " x: " + x + " y: " + y);

        FrameLayout.LayoutParams layoutParams = AATKitHelper.getLayoutParamsFromAlignment(bannerAlignment);
        setOffsetForLayoutParams(layoutParams, x, y);

        cordova.getActivity().runOnUiThread(() -> {

            if (AATKitHelper.isInitialized() && stickyBannerPlacements.containsKey(placementName)) {
                StickyBannerPlacement bannerPlacement = stickyBannerPlacements.get(placementName);
                View placementView = bannerPlacement.getPlacementView();
                placementView.setLayoutParams(layoutParams);
            }
        });
    }

    private void setOffsetForLayoutParams(FrameLayout.LayoutParams layoutParams, int x, int y) {
        if(x > 0) {
            layoutParams.leftMargin = x;
        } else if(x < 0) {
            layoutParams.rightMargin = -x;
        }

        if (y > 0) {
            layoutParams.topMargin = y;
        } else if (y < 0) {
            layoutParams.bottomMargin = -y;
        }
    }

    private void setPlacementPosition(JSONArray args) throws JSONException {

        String placementName = args.getString(0);
        int posX = args.getInt(1);
        int posY = args.getInt(2);

        log("setPlacementPosition: placementName: " + placementName + ", posX: " + posX + ", posY: " + posY);

        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
        layoutParams.leftMargin = posX;
        layoutParams.topMargin = posY;

        cordova.getActivity().runOnUiThread(() -> {

            if (AATKitHelper.isInitialized() && stickyBannerPlacements.containsKey(placementName)) {
                StickyBannerPlacement bannerPlacement = stickyBannerPlacements.get(placementName);
                View placementView = bannerPlacement.getPlacementView();
                placementView.setLayoutParams(layoutParams);
            }
        });
    }

    private void setPlacementContentGravity(JSONArray args) throws JSONException {

        String placementName = args.getString(0);
        String gravity = args.getString(1);

        log("setPlacementContentGravity: placementName: " + placementName + ", gravity: " + gravity);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized() && stickyBannerPlacements.containsKey(placementName)) {
                int placementGravity = Gravity.BOTTOM;

                switch (gravity.toLowerCase()) {
                    case "top":
                        placementGravity = Gravity.TOP;
                        break;
                    case "bottom":
                        placementGravity = Gravity.BOTTOM;
                        break;
                    case "center":
                        placementGravity = Gravity.CENTER;
                        break;
                }

                StickyBannerPlacement bannerPlacement = stickyBannerPlacements.get(placementName);
                bannerPlacement.setContentGravity(placementGravity);

            }
        });
    }

    // endregion AATKit methods - banners

    // region AATKit methods - fullscreens

    private void showPlacement(JSONArray args, CallbackContext callback) throws JSONException {

        String placementName = args.getString(0);

        log("showPlacement: placementName: " + placementName);

        cordova.getActivity().runOnUiThread(() -> {
            boolean success = false;

            if(fullscreenPlacements.containsKey(placementName)) {
                FullscreenPlacement placement = fullscreenPlacements.get(placementName);
                success = placement.show();
            } else if(rewardedVideoPlacements.containsKey(placementName)) {
                RewardedVideoPlacement placement = rewardedVideoPlacements.get(placementName);
                success = placement.show();
            }  else if(appOpenPlacements.containsKey(placementName)) {
                AppOpenAdPlacement placement = appOpenPlacements.get(placementName);
                success = placement.show();
            }else {
                logWarn("Placement " + placementName + " does not exist.");
            }
            if (callback != null) {
                callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, success));
            }
        });

    }

    // endregion AATKit methods - fullscreens

    // region AATKit methods - targeting

    private void setTargetingInfo(JSONArray args) throws JSONException {
        JSONObject infoJson = args.getJSONObject(0);

        log("setTargetingInfo: infoJson: " + infoJson);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                try {
                    Map<String, List<String>> info = convertJsonToTargetingInfo(infoJson);
                    AATKit.setTargetingInfo(info);
                } catch (JSONException e) {
                    logError(e.getMessage());
                }
            }
        });
    }

    private void setTargetingInfoForPlacement(JSONArray args) throws JSONException {
        String placementName = args.getString(0);
        JSONObject infoJson = args.getJSONObject(1);

        log("setTargetingInfoForPlacement: placementName: " + placementName + " infoJson: " + infoJson);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                try {
                    Map<String, List<String>> info = convertJsonToTargetingInfo(infoJson);

                    if(stickyBannerPlacements.containsKey(placementName)) {
                        StickyBannerPlacement placement = stickyBannerPlacements.get(placementName);
                        placement.setTargetingInfo(info);
                    } else if(fullscreenPlacements.containsKey(placementName)) {
                        FullscreenPlacement placement = fullscreenPlacements.get(placementName);
                        placement.setTargetingInfo(info);
                    } else if(rewardedVideoPlacements.containsKey(placementName)) {
                        RewardedVideoPlacement placement = rewardedVideoPlacements.get(placementName);
                        placement.setTargetingInfo(info);
                    }  else if(appOpenPlacements.containsKey(placementName)) {
                        AppOpenAdPlacement placement = appOpenPlacements.get(placementName);
                        placement.setTargetingInfo(info);
                    }
                } catch (JSONException e) {
                    logError(e.getMessage());
                }
            }
        });
    }

    private Map<String, List<String>> convertJsonToTargetingInfo(JSONObject json) throws JSONException {
        Map<String, List<String>> result = new HashMap<>();

        Iterator<String> keys = json.keys();
        while (keys.hasNext()) {
            String key = keys.next();
            if (json.get(key) instanceof JSONArray) {
                JSONArray array = json.getJSONArray(key);
                List<String> values = createValues(array);
                result.put(key, values);
            }
        }

        return result;
    }

    private List<String> createValues(JSONArray array) throws JSONException {
        int arraySize = array.length();
        List<String> values = new ArrayList<>(arraySize);
        for (int i = 0; i < arraySize; i++) {
            values.add(array.getString(i));
        }
        return values;
    }

    private void setContentTargetingUrl(JSONArray args) throws JSONException {
        String url = args.getString(0);

        log("setContentTargetingUrl: url: " + url);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                AATKit.setContentTargetingUrl(url);
            }
        });
    }

    private void setContentTargetingUrlForPlacement(JSONArray args) throws JSONException {
        String placementName = args.getString(0);
        String url = args.getString(1);

        log("setContentTargetingUrl: placementName: " + placementName + " url: " + url);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                if(stickyBannerPlacements.containsKey(placementName)) {
                    StickyBannerPlacement placement = stickyBannerPlacements.get(placementName);
                    placement.setContentTargetingUrl(url);
                } else if(fullscreenPlacements.containsKey(placementName)) {
                    FullscreenPlacement placement = fullscreenPlacements.get(placementName);
                    placement.setContentTargetingUrl(url);
                } else if(rewardedVideoPlacements.containsKey(placementName)) {
                    RewardedVideoPlacement placement = rewardedVideoPlacements.get(placementName);
                    placement.setContentTargetingUrl(url);
                }  else if(appOpenPlacements.containsKey(placementName)) {
                    AppOpenAdPlacement placement = appOpenPlacements.get(placementName);
                    placement.setContentTargetingUrl(url);
                }
            }
        });
    }

    private void addAdNetworkForKeywordTargeting(JSONArray args) throws JSONException {

        String networkName = args.getString(0);

        log("addAdNetworkForKeywordTargeting: networkName: " + networkName);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                AdNetwork adNetwork = AATKitHelper.getAdNetworkFromName(networkName);

                if (adNetwork != null) {
                    AATKit.addAdNetworkForKeywordTargeting(adNetwork);
                } else {
                    log(networkName + " is not supported on this platform.");
                }
            }
        });
    }

    private void removeAdNetworkForKeywordTargeting(JSONArray args) throws JSONException {

        String networkName = args.getString(0);

        log("removeAdNetworkForKeywordTargeting: networkName" + networkName);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                AdNetwork adNetwork = AATKitHelper.getAdNetworkFromName(networkName);

                if (adNetwork != null) {
                    AATKit.removeAdNetworkForKeywordTargeting(adNetwork);
                } else {
                    log(networkName + " is not supported on this platform.");
                }
            }
        });
    }

    // endregion AATKit methods - targeting

    // endregion AATKit methods

    private void setRuntimeConfiguration(AATKitAbstractConfiguration runtimeConfiguration, JSONObject jsonObject) throws JSONException {

        if (jsonObject.has(AATKIT_CONFIGURATION_USEGEOLOCATION))
            runtimeConfiguration.setUseGeoLocation(jsonObject.getBoolean(AATKIT_CONFIGURATION_USEGEOLOCATION));

        if (jsonObject.has(AATKIT_CONFIGURATION_CONSENTREQUIRED))
            runtimeConfiguration.setConsentRequired(jsonObject.getBoolean(AATKIT_CONFIGURATION_CONSENTREQUIRED));

        if (jsonObject.has(AATKIT_CONFIGURATION_CONSENT)) {

            JSONObject joConfigurationConsent = jsonObject.getJSONObject(AATKIT_CONFIGURATION_CONSENT);
            if (joConfigurationConsent.has(AATKIT_CONSENT_TYPE)) {
                setConsent(runtimeConfiguration, joConfigurationConsent);
            }
        }
    }

    private void setConsent(AATKitAbstractConfiguration runtimeConfiguration, JSONObject joConfigurationConsent) throws JSONException {
        String consnetType = joConfigurationConsent.getString(AATKIT_CONSENT_TYPE);

        if (consnetType.equalsIgnoreCase(AATKIT_CONSENT_TYPE_SIMPLECONSENT)) {
            setSimpleConsent(runtimeConfiguration, joConfigurationConsent);
        } else if (consnetType.equalsIgnoreCase(AATKit_CONSENT_TYPE_MANAGED_CMP_GOOGLE)) {
            setManagedConsentCMPGoogle(runtimeConfiguration, joConfigurationConsent);
        } else if(consnetType.equalsIgnoreCase(AATKit_CONSENT_TYPE_MANAGED_CMP_OGURY)) {
            setManagedConsentCMPOgury(runtimeConfiguration, joConfigurationConsent);
        } else if(consnetType.equalsIgnoreCase(AATKit_CONSENT_TYPE_MANAGED_CMP_SOURCE_POINT)) {
            setManagedConsentCMPSourcePoint(runtimeConfiguration, joConfigurationConsent);
        } else if(consnetType.equalsIgnoreCase(AATKIT_CONSENT_TYPE_VENDOR)) {
            setVendorConsent(runtimeConfiguration, joConfigurationConsent);
        }

        setNoConsentNetworkStopSetIfNeeded(joConfigurationConsent);
    }

    private void setSimpleConsent(AATKitAbstractConfiguration runtimeConfiguration, JSONObject joConfigurationConsent) throws JSONException {
        if (joConfigurationConsent.has(AATKIT_CONSENT_NONIAB)) {
            String nonIABConsent = joConfigurationConsent.getString(AATKIT_CONSENT_NONIAB);
            NonIABConsent nativeNonIABConsent = getNonIABConsent(nonIABConsent);
            consent = new SimpleConsent(nativeNonIABConsent);
            runtimeConfiguration.setConsent(consent);
        }
    }

    private void setManagedConsentCMPGoogle(AATKitAbstractConfiguration runtimeConfiguration, JSONObject joConfigurationConsent) throws JSONException {
        Activity activity = cordova.getActivity();
        CMP cmp = new CMPGoogle(activity);
        ManagedConsent.ShowIfNeededSetting showIfNeededSetting = getShowIfNeededSetting(joConfigurationConsent);
        managedConsent = new ManagedConsent(cmp, activity, createManagedConsentDelegate(), showIfNeededSetting);
        consent = managedConsent;
        runtimeConfiguration.setConsent(managedConsent);
    }

    private void setManagedConsentCMPOgury(AATKitAbstractConfiguration runtimeConfiguration, JSONObject joConfigurationConsent) throws JSONException {
        Activity activity = cordova.getActivity();
        String assetKey = joConfigurationConsent.has(AATKIT_CONSENT_ASSET_KEY_CMP_OGURY) ? joConfigurationConsent.getString(AATKIT_CONSENT_ASSET_KEY_CMP_OGURY) : "";
        CMP cmp = new CMPOgury(activity.getApplication(), assetKey);
        ManagedConsent.ShowIfNeededSetting showIfNeededSetting = getShowIfNeededSetting(joConfigurationConsent);
        managedConsent = new ManagedConsent(cmp, activity, createManagedConsentDelegate(), showIfNeededSetting);
        consent = managedConsent;
        runtimeConfiguration.setConsent(managedConsent);
    }

    private void setManagedConsentCMPSourcePoint(AATKitAbstractConfiguration runtimeConfiguration, JSONObject joConfigurationConsent) throws JSONException {
        Activity activity = cordova.getActivity();
        int yourAccountID = joConfigurationConsent.has(AATKIT_CONSENT_YOUR_ACCOUNT_ID) ? joConfigurationConsent.getInt(AATKIT_CONSENT_YOUR_ACCOUNT_ID) : -1;
        int yourPropertyId = joConfigurationConsent.has(AATKIT_CONSENT_YOUR_PROPERTY_ID) ? joConfigurationConsent.getInt(AATKIT_CONSENT_YOUR_PROPERTY_ID) : -1;
        String yourPropertyName = joConfigurationConsent.has(AATKIT_CONSENT_YOUR_PROPERTY_NAME) ? joConfigurationConsent.getString(AATKIT_CONSENT_YOUR_PROPERTY_NAME) : "";
        String yourPMId = joConfigurationConsent.has(AATKIT_CONSENT_YOUR_PM_ID) ? joConfigurationConsent.getString(AATKIT_CONSENT_YOUR_PM_ID) : "";
        CMP cmp = new CMPSourcePoint(activity, yourAccountID, yourPropertyId, yourPropertyName, yourPMId);
        ManagedConsent.ShowIfNeededSetting showIfNeededSetting = getShowIfNeededSetting(joConfigurationConsent);
        managedConsent = new ManagedConsent(cmp, activity, createManagedConsentDelegate(), showIfNeededSetting);
        consent = managedConsent;
        runtimeConfiguration.setConsent(managedConsent);
    }

    private static ManagedConsent.ShowIfNeededSetting getShowIfNeededSetting(JSONObject joConfigurationConsent) throws JSONException {
        String showIfNeededSettingRaw = joConfigurationConsent.has(AATKIT_SHOW_IF_NEEDED_SETTING) ? joConfigurationConsent.getString(AATKIT_SHOW_IF_NEEDED_SETTING) : "";
        ManagedConsent.ShowIfNeededSetting showIfNeededSetting = ManagedConsent.ShowIfNeededSetting.SERVER_SIDE_CONTROL;
        if(showIfNeededSettingRaw.equalsIgnoreCase(AATKIT_SHOW_IF_NEEDED_SETTING_ALWAYS)){
            showIfNeededSetting = ManagedConsent.ShowIfNeededSetting.ALWAYS;
        } else if(showIfNeededSettingRaw.equalsIgnoreCase(AATKIT_SHOW_IF_NEEDED_SETTING_NEVER)) {
            showIfNeededSetting = ManagedConsent.ShowIfNeededSetting.NEVER;
        } else if(showIfNeededSettingRaw.equalsIgnoreCase(AATKIT_SHOW_IF_NEEDED_SETTING_SERVER)) {
            showIfNeededSetting = ManagedConsent.ShowIfNeededSetting.SERVER_SIDE_CONTROL;
        }
        return showIfNeededSetting;
    }

    private void setVendorConsent(AATKitAbstractConfiguration runtimeConfiguration, JSONObject joConfigurationConsent) throws JSONException {
        consent = new VendorConsent(createVendorDelegate());
        runtimeConfiguration.setConsent(consent);
        setConsentForAddapptr(joConfigurationConsent);
        setObtainedNetworks(joConfigurationConsent);
    }

    private void setConsentForAddapptr(JSONObject joConfigurationConsent) throws JSONException {
        if(joConfigurationConsent.has(AATKIT_VENDOR_CONSENT_FOR_ADDAPPTR)) {
            String nonIABConsent = joConfigurationConsent.getString(AATKIT_VENDOR_CONSENT_FOR_ADDAPPTR);
            consentForAddapptr = getNonIABConsent(nonIABConsent);
        }
    }

    private void setObtainedNetworks(JSONObject joConfigurationConsent) throws JSONException {
        if(joConfigurationConsent.has(AATKIT_VENDOR_CONSENT_OBTAINED_NETWORKS)) {
            vendorConsentObtainedNetworks.clear();
            JSONArray obtainedNetworks =  joConfigurationConsent.getJSONArray(AATKIT_VENDOR_CONSENT_OBTAINED_NETWORKS);

            for(int i=0; i<obtainedNetworks.length(); i++) {
                String networkName = obtainedNetworks.getString(i);
                AdNetwork adNetwork = AATKitHelper.getAdNetworkFromName(networkName);
                vendorConsentObtainedNetworks.add(adNetwork);
            }
        }
    }

    private void setNoConsentNetworkStopSetIfNeeded(JSONObject joConfigurationConsent) throws JSONException {
        if(joConfigurationConsent.has(AATKIT_NO_CONSENT_NETWORK_STOP_SET) && managedConsent!= null) {
            JSONArray networks = joConfigurationConsent.getJSONArray(AATKIT_NO_CONSENT_NETWORK_STOP_SET);
            Set<AdNetwork> noConsentNetworkStopSet = new HashSet<>();
            for(int i=0; i<networks.length(); i++) {
                String networkName = networks.getString(i);
                AdNetwork adNetwork = AATKitHelper.getAdNetworkFromName(networkName);
                noConsentNetworkStopSet.add(adNetwork);
            }

            if(!noConsentNetworkStopSet.isEmpty() && consent != null) {
                consent.setNoConsentNetworkStopSet(noConsentNetworkStopSet);
            }
        }
    }

    private void setPublisherProvidedId(JSONArray args) throws JSONException {
        String providedId = args.getString(0);

        log("setPublisherProvidedId: providedId: " + providedId);

        cordova.getActivity().runOnUiThread(() -> {

            if (AATKitHelper.isInitialized()) {
                AATKit.setPublisherProvidedId(providedId);
            }
        });
    }

    private VendorConsent.VendorConsentDelegate createVendorDelegate() {
        return new VendorConsent.VendorConsentDelegate() {
            @Override
            public NonIABConsent getConsentForNetwork(AdNetwork adNetwork) {
                if(vendorConsentObtainedNetworks.contains(adNetwork)) {
                    return NonIABConsent.OBTAINED;
                }

                return NonIABConsent.WITHHELD;
            }

            @Override
            public NonIABConsent getConsentForAddapptr() {
                return consentForAddapptr;
            }
        };
    }

    private NonIABConsent getNonIABConsent(String nonIABCOnsent) {
        NonIABConsent nativeNonIABConsent = NonIABConsent.UNKNOWN;

        if (nonIABCOnsent.equalsIgnoreCase(AATKIT_CONSENT_NONIAB_UNKNOWN))
            nativeNonIABConsent = NonIABConsent.UNKNOWN;
        else if (nonIABCOnsent.equalsIgnoreCase(AATKIT_CONSENT_NONIAB_WITHHELD))
            nativeNonIABConsent = NonIABConsent.WITHHELD;
        else if (nonIABCOnsent.equalsIgnoreCase(AATKIT_CONSENT_NONIAB_OBTAINED))
            nativeNonIABConsent = NonIABConsent.OBTAINED;

        return nativeNonIABConsent;
    }

    private AATKit.StatisticsListener createStatisticsListener(String placementName) {
        AATKit.StatisticsListener statisticsListener = new AATKit.StatisticsListener() {
            @Override
            public void countedMediationCycle(Placement placement) {
                webView.loadUrl(
                        String.format(
                                "javascript:cordova.fireDocumentEvent('countedMediationCycle', { 'placementName': '%s' });",
                                placementName));
            }

            @Override
            public void countedAdSpace(Placement placement) {
                webView.loadUrl(
                        String.format(
                                "javascript:cordova.fireDocumentEvent('countedAdSpace', { 'placementName': '%s' });",
                                placementName));
            }

            @Override
            public void countedRequest(Placement placement, AdNetwork network) {
                fireStatisticsEvent("countedRequest", placementName, network);
            }

            @Override
            public void countedResponse(Placement placement, AdNetwork network) {
                fireStatisticsEvent("countedResponse", placementName, network);
            }

            @Override
            public void countedImpression(Placement placement, AdNetwork network) {
                fireStatisticsEvent("countedImpression", placementName, network);
            }

            @Override
            public void countedVimpression(Placement placement, AdNetwork network) {
                fireStatisticsEvent("countedVimpression", placementName, network);
            }

            @Override
            public void countedNimpression(@NonNull Placement placement, @NonNull AdNetwork network) {
                fireStatisticsEvent("countedNimpression", placementName, network);
            }

            @Override
            public void countedDirectDealImpression(Placement placement, AdNetwork network) {
                fireStatisticsEvent("countedDirectDealImpression", placementName, network);
            }

            @Override
            public void countedClick(Placement placement, AdNetwork network) {
                fireStatisticsEvent("countedClick", placementName, network);
            }
        };
        return statisticsListener;
    }

    private void fireStatisticsEvent(String eventName, String placementName, AdNetwork network) {
        String networkName = AATKitHelper.getNameFromAdNetwork(network);
        webView.loadUrl(
                String.format(
                        "javascript:cordova.fireDocumentEvent('%s', { 'placementName': '%s', 'network': '%s' });",
                        eventName,
                        placementName,
                        networkName));
    }

    private void isConsentOptIn(CallbackContext callback) {
        log(AATKIT_IS_CONSENT_OPT_IN);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                final boolean isConsentOptIn = AATKit.isConsentOptIn();
                callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, isConsentOptIn));
            }
        });
    }

    private void isTablet(CallbackContext callback) throws JSONException {
        log(IS_TABLET);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                final boolean isTablet = AATKit.isTablet(cordova.getContext());
                callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, isTablet));
            }
        });
    }

    private void maximumBannerSizePortrait(CallbackContext callback) throws JSONException {
        log(MAXIMUM_BANNER_SIZE_PORTRAIT);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                final BannerPlacementSize placementSize = AATKit.maximumBannerSizePortrait(cordova.getContext());
                String size = convertPlacementSizeToString(placementSize);
                callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, size));
            }
        });
    }

    private void maximumBannerSizeLandscape(CallbackContext callback) throws JSONException {
        log(MAXIMUM_BANNER_SIZE_LANDSCAPE);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                final BannerPlacementSize placementSize = AATKit.maximumBannerSizeLandscape(cordova.getContext());
                String size = convertPlacementSizeToString(placementSize);
                callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, size));
            }
        });
    }

    private String convertPlacementSizeToString(BannerPlacementSize placementSize) {
        String size = "unknown";
        switch (placementSize) {
            case Banner320x53:
                size = "Banner320x53";
                break;
            case Banner320x50:
                size = "Banner320x50";
                break;
            case Banner768x90:
                size = "Banner768x90";
                break;
            case Banner300x250:
                size = "Banner300x250";
                break;
            case Banner468x60:
                size = "Banner468x60";
                break;
            case Banner320x75:
                size = "Banner320x75";
                break;
            case Banner320x100:
                size = "Banner320x100";
                break;
            case Banner320x150:
                size = "Banner320x150";
                break;
            case Banner320x160:
                size = "Banner320x160";
                break;
            case Banner320x480:
                size = "Banner320x480";
                break;
        }
        return size;
    }

    private void fittingBannerSizesPortrait(CallbackContext callback) throws JSONException {
        log(FITTING_BANNER_SIZES_PORTRAIT);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                final Set<BannerSize> bannerSizes = AATKit.fittingBannerSizesPortrait(cordova.getContext());
                final JSONArray sizes = convertBannerSizesToArray(bannerSizes);
                callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, sizes));
            }
        });
    }

    private void fittingBannerSizesLandscape(CallbackContext callback) throws JSONException {
        log(FITTING_BANNER_SIZES_LANDSCAPE);

        cordova.getActivity().runOnUiThread(() -> {
            if (AATKitHelper.isInitialized()) {
                final Set<BannerSize> bannerSizes = AATKit.fittingBannerSizesLandscape(cordova.getContext());
                final JSONArray sizes = convertBannerSizesToArray(bannerSizes);
                callback.sendPluginResult(new PluginResult(PluginResult.Status.OK, sizes));
            }
        });
    }

    private JSONArray convertBannerSizesToArray(final Set<BannerSize> bannerSizes) {
        final JSONArray sizes = new JSONArray();
        for(BannerSize bannerSize : bannerSizes) {
            switch (bannerSize) {
                case Banner320x53:
                    sizes.put("Banner320x53");
                    break;
                case Banner768x90:
                    sizes.put("Banner768x90");
                    break;
                case Banner300x250:
                    sizes.put("Banner300x250");
                    break;
                case Banner468x60:
                    sizes.put("Banner468x60");
                    break;
                case MultipleSizes:
                    sizes.put("MultipleSizes");
                    break;
                case Banner320x75:
                    sizes.put("Banner320x75");
                    break;
                case Banner320x100:
                    sizes.put("Banner320x100");
                    break;
                case Banner320x150:
                    sizes.put("Banner320x150");
                    break;
                case Banner320x160:
                    sizes.put("Banner320x160");
                    break;
                case Banner320x480:
                    sizes.put("Banner320x480");
                    break;
            }
        }
        return sizes;
    }

    @Override
    public void onPause(boolean multitasking) {
        super.onPause(multitasking);

        if (AATKitHelper.isInitialized()) {
            AATKit.onActivityPause(cordova.getActivity());
        }
    }

    @Override
    public void onResume(boolean multitasking) {
        super.onResume(multitasking);

        if (AATKitHelper.isInitialized()) {
            AATKit.onActivityResume(cordova.getActivity());
        }
    }

    private AATKit.Delegate createDelegate() {
        return new AATKit.Delegate() {
            @Override
            public void aatkitObtainedAdRules(boolean fromTheServer) {
                webView.loadUrl(String.format("javascript:cordova.fireDocumentEvent('aatkitObtainedAdRules', { 'fromTheServer': %b });", fromTheServer));
            }

            @Override
            public void aatkitUnknownBundleId() {
                webView.loadUrl("javascript:cordova.fireDocumentEvent('aatkitUnknownBundleId');");
            }
        };
    }

    private ManagedConsent.ManagedConsentDelegate createManagedConsentDelegate() {
        return new ManagedConsent.ManagedConsentDelegate() {

            @Override
            public void managedConsentNeedsUserInterface(ManagedConsent managedConsent) {
                managedConsentNeedingUI = managedConsent;
                webView.loadUrl("javascript:cordova.fireDocumentEvent('managedConsentNeedsUserInterface');");
            }

            @Override
            public void managedConsentCMPFinished(ManagedConsent.ManagedConsentState state) {
                String stateParam = "";

                switch(state) {
                    case OBTAINED:
                        stateParam = "obtained";
                        break;
                    case WITHHELD:
                        stateParam = "withheld";
                        break;
                    case CUSTOM:
                        stateParam = "custom";
                        break;
                    case UNKNOWN:
                    default:
                        stateParam = "unknown";
                        break;
                }

                webView.loadUrl(String.format("javascript:cordova.fireDocumentEvent('managedConsentCMPFinished', { 'state': '%s' });", stateParam));
            }

            @Override
            public void managedConsentCMPFailedToLoad(ManagedConsent managedConsent, String error) {
                webView.loadUrl(String.format("javascript:cordova.fireDocumentEvent('managedConsentCMPFailedToLoad', { 'error': '%s' });", error));
            }

            @Override
            public void managedConsentCMPFailedToShow(ManagedConsent managedConsent, String error) {
                webView.loadUrl(String.format("javascript:cordova.fireDocumentEvent('managedConsentCMPFailedToShow', { 'error': '%s' });", error));
            }
        };
    }

    private void log(String message) {
        if (debugLog) {
            log(false, message);
        }
    }

    private void log(Boolean forceLog, String message) {
        if (forceLog || debugLog) {
            Log.d(TAG, message);
        }
    }

    private void logWarn(String message) {
        Log.w(TAG, message);
    }

    private void logError(String message) {
        Log.e(TAG, message);
    }
}