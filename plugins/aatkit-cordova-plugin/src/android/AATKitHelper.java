package com.intentsoftware.addapptr;

import android.view.Gravity;
import android.widget.FrameLayout.LayoutParams;

import com.intentsoftware.addapptr.internal.PluginVersioningTool;

public class AATKitHelper {

    public static boolean isInitialized() {
        return AATKit.isInitialized();
    }

    public static void destroy() {
        AATKit.destroy();
    }

    public static void setPluginVersion(String version) {
        PluginVersioningTool.appendPluginInformation(PluginVersioningTool.PluginName.Cordova, version);
    }

    public static AdNetwork getAdNetworkFromName(String name) {
        switch (name.toLowerCase()) {
            case "adcolony":
                return AdNetwork.ADCOLONY;
            case "admob":
                return AdNetwork.ADMOB;
            case "rtb2":
                return AdNetwork.RTB2;
            case "amazonhb":
                return AdNetwork.AMAZONHB;
            case "applovin":
                return AdNetwork.APPLOVIN;
            case "applovinmax":
                return AdNetwork.APPLOVINMAX;
            case "appnexus":
                return AdNetwork.APPNEXUS;
            case "bluestack":
                return AdNetwork.BLUESTACK;
            case "criteosdk":
                return AdNetwork.CRITEOSDK;
            case "dfp":
                return AdNetwork.DFP;
            case "dfpdirect":
                return AdNetwork.DFPDIRECT;
            case "empty":
                return AdNetwork.EMPTY;
            case "facebook":
                return AdNetwork.FACEBOOK;
            case "feedad":
                return AdNetwork.FEEDAD;
            case "gravitertb":
                return AdNetwork.GRAVITERTB;
            case "huawei":
                return AdNetwork.HUAWEI;
            case "inmobi":
                return AdNetwork.INMOBI;
            case "ironsource":
                return AdNetwork.IRONSOURCE;
            case "kidoz":
                return AdNetwork.KIDOZ;
            case "mintegral":
                return AdNetwork.MINTEGRAL;
            case "ogury":
                return AdNetwork.OGURY;
            case "pubnative":
                return AdNetwork.PUBNATIVE;
            case "smaato":
                return AdNetwork.SMAATO;
            case "smartad":
                return AdNetwork.SMARTAD;
            case "smartadserverdirect":
                return AdNetwork.SMARTADSERVERDIRECT;
            case "tappx":
                return AdNetwork.TAPPX;
            case "teads":
                return AdNetwork.TEADS;
            case "unity":
                return AdNetwork.UNITY;
            case "vungle2":
                return AdNetwork.VUNGLE2;
            case "yoc":
                return AdNetwork.YOC;
            default:
                return null;
        }
    }

    public static String getNameFromAdNetwork(AdNetwork network) {
        switch (network) {
            case ADCOLONY:
                return "ADCOLONY";
            case ADMOB:
                return "ADMOB";
            case RTB2:
                return "RTB2";
            case AMAZONHB:
                return "AMAZONHB";
            case APPLOVIN:
                return "APPLOVIN";
            case APPLOVINMAX:
                return "APPLOVINMAX";
            case APPNEXUS:
                return "APPNEXUS";
            case BLUESTACK:
                return "BLUESTACK";
            case CRITEOSDK:
                return "CRITEOSDK";
            case DFP:
                return "DFP";
            case DFPDIRECT:
                return "DFPDIRECT";
            case EMPTY:
                return "EMPTY";
            case FACEBOOK:
                return "FACEBOOK";
            case FEEDAD:
                return "FEEDAD";
            case GRAVITERTB:
                return "GRAVITERTB";
            case HUAWEI:
                return "HUAWEI";
            case INMOBI:
                return "INMOBI";
            case IRONSOURCE:
                return "IRONSOURCE";
            case KIDOZ:
                return "KIDOZ";
            case MINTEGRAL:
                return "MINTEGRAL";
            case OGURY:
                return "OGURY";
            case PUBNATIVE:
                return "PUBNATIVE";
            case SMAATO:
                return "SMAATO";
            case SMARTAD:
                return "SMARTAD";
            case SMARTADSERVERDIRECT:
                return "SMARTADSERVERDIRECT";
            case TAPPX:
                return "TAPPX";
            case TEADS:
                return "TEADS";
            case UNITY:
                return "UNITY";
            case VUNGLE2:
                return "VUNGLE2";
            case YOC:
                return "YOC";
            default:
                return "UNKNOWN";
        }
    }

    public static LayoutParams getLayoutParamsFromAlignment(String bannerAlignment) {

        LayoutParams layoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);

        switch (bannerAlignment.toLowerCase()) {
            case "topleft":
                layoutParams.gravity = Gravity.LEFT | Gravity.TOP;
                break;
            case "topcenter":
                layoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;
                break;
            case "topright":
                layoutParams.gravity = Gravity.RIGHT | Gravity.TOP;
                break;
            case "bottomleft":
                layoutParams.gravity = Gravity.LEFT | Gravity.BOTTOM;
                break;
            case "bottomcenter":
                layoutParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM;
                break;
            case "bottomright":
                layoutParams.gravity = Gravity.RIGHT | Gravity.BOTTOM;
                break;
        }
        return layoutParams;
    }
}