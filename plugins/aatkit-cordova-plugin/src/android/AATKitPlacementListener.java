package com.addaptr.cordovaplugin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.intentsoftware.addapptr.AATKitReward;
import com.intentsoftware.addapptr.AppOpenPlacementListener;
import com.intentsoftware.addapptr.FullscreenPlacementListener;
import com.intentsoftware.addapptr.Placement;
import com.intentsoftware.addapptr.RewardedVideoPlacementListener;
import com.intentsoftware.addapptr.StickyBannerPlacementListener;

import org.apache.cordova.CordovaWebView;

public class AATKitPlacementListener implements StickyBannerPlacementListener, FullscreenPlacementListener, RewardedVideoPlacementListener, AppOpenPlacementListener {
    private final String placementName;
    private final CordovaWebView webView;

    public AATKitPlacementListener(String placementName, CordovaWebView webView) {
        this.placementName = placementName;
        this.webView = webView;
    }

    @Override
    public void onPauseForAd(@NonNull Placement placement) {
        webView.loadUrl(String.format("javascript:cordova.fireDocumentEvent('aatkitPauseForAd', { 'placementName': '%s' });", placementName));
    }

    @Override
    public void onResumeAfterAd(@NonNull Placement placement) {
        webView.loadUrl(String.format("javascript:cordova.fireDocumentEvent('aatkitResumeAfterAd', { 'placementName': '%s' });", placementName));
    }

    @Override
    public void onHaveAd(@NonNull Placement placement) {
        webView.loadUrl(String.format("javascript:cordova.fireDocumentEvent('aatkitHaveAd', { 'placementName': '%s' });", placementName));
    }

    @Override
    public void onNoAd(@NonNull Placement placement) {
        webView.loadUrl(String.format("javascript:cordova.fireDocumentEvent('aatkitNoAd', { 'placementName': '%s' });", placementName));
    }

    @Override
    public void onUserEarnedIncentive(@NonNull Placement placement, @Nullable AATKitReward reward) {
        if (reward != null) {
            webView.loadUrl(String.format("javascript:cordova.fireDocumentEvent('aatkitUserEarnedIncentive', { 'placementName': '%s', 'reward': { 'name': '%s', 'value': '%s' } });", placementName, reward.getName(), reward.getValue()));
        } else {
            webView.loadUrl(String.format("javascript:cordova.fireDocumentEvent('aatkitUserEarnedIncentive', { 'placementName': '%s' });", placementName));
        }
    }
}
