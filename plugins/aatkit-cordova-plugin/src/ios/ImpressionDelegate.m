#import "ImpressionDelegate.h"
#import "StatisticsDelegate.h"

@implementation ImpressionDelegate

-(id)initWithPlacementName:(NSString*)placementName andCordovaPlguin:(CDVPlugin*)cordovaPlugin {
    self = [super init];
    self.placementName = placementName;
    self.cordovaPlugin = cordovaPlugin;
    return self;
}

- (void)didCountImpressionWithPlacement:(id <AATPlacement> _Nullable)placement :(AATImpression * _Nonnull)impression {
    NSString* data = [NSString stringWithFormat:@"{placementName:'%@', bannerSize:'%@', mediationTypeName:'%@', mediationType:'%@', adNetworkName:'%@', networkKey:'%@', adNetwork:'%@', price:'%f', currencyCode:'%@', precision:'%@'}",
                      self.placementName,
                      impression.bannerSize != nil ? impression.bannerSize : @"",
                      [self getMediationType:impression],
                      [self getMediationType:impression],
                      impression.networkKey != nil ? impression.networkKey : @"",
                      impression.networkKey != nil ? impression.networkKey : @"",
                      [StatisticsDelegate convertNetworkToString:impression.adNetwork],
                      impression.price,
                      impression.currencyCode != nil ? impression.currencyCode : @"",
                      [self getPrecisionType:impression]];
    NSString* event = [NSString stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@',%@);", @"didCountImpression", data];
    [self.cordovaPlugin.commandDelegate evalJs:event];
}

- (NSString *)getMediationType:(AATImpression * _Nonnull)impression {
    NSString* mediationType;
    switch(impression.mediationType) {
        case AATMediationTypeMAYO:
            mediationType = @"MAYO";
            break;
        case AATMediationTypeAUCTION:
            mediationType = @"AUCTION";
            break;
        case AATMediationTypeWATERFALL:
            mediationType = @"WATERFALL";
            break;
        default:
            mediationType = @"";
            break;
    }
    return mediationType;
}

- (NSString *)getPrecisionType:(AATImpression * _Nonnull)impression {
    NSString* precisionType;
    switch(impression.precision) {
        case AATImpressionPricePrecisionTypeUnknown:
            precisionType = @"UNKNOWN";
            break;
        case AATImpressionPricePrecisionTypePrecise:
            precisionType = @"PRECISE";
            break;
        case AATImpressionPricePrecisionTypeEstimated:
            precisionType = @"ESTIMATED";
            break;
        case AATImpressionPricePrecisionTypePublisherProvided:
            precisionType = @"PUBLISHER_PROVIDED";
            break;
        case AATImpressionPricePrecisionTypeFloorPrice:
            precisionType = @"FLOOR_PRICE";
            break;
        default:
            precisionType = @"";
            break;
    }
    return precisionType;
}

@end
