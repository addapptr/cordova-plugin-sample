#import <AATKit/AATKit.h>
#import <Cordova/CDV.h>

@interface StatisticsDelegate : NSObject <AATStatisticsDelegate>

@property (nonatomic, weak) CDVPlugin* cordovaPlugin;
@property (nonatomic, retain) NSString* placementName;

-(id)initWithPlacementName:(NSString*)placementName andCordovaPlugin:(CDVPlugin*)cordovaPlugin;

- (void)AATKitCountedAdSpace;
- (void)AATKitCountedRequestFor:(AATAdNetwork)network;
- (void)AATKitCountedResponseFor:(AATAdNetwork)network;
- (void)AATKitCountedImpressionFor:(AATAdNetwork)network;
- (void)AATKitCountedVImpressionFor:(AATAdNetwork)network;
- (void)AATKitCountedClickFor:(AATAdNetwork)network;
- (void)AATKitCountedDirectDealImpressionFor:(AATAdNetwork)network;
- (void)AATKitCountedMediationCycle;

+(NSString*) convertNetworkToString:(int) network;

@end
