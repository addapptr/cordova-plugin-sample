#import "AATKitDelegate.h"

@implementation AATKitDelegate

- (id)initWithPlacementName:(NSString*)placementName andCordovaPlguin:(CDVPlugin*)cordovaPlugin {
    self = [super init];
    self.placementName = placementName;
    self.cordovaPlugin = cordovaPlugin;
    return self;
}

- (void)aatPauseForAdWithPlacement:(id <AATPlacement> _Nonnull)placement {
    [self fireEventWithPlacementName:self.placementName eventName:@"aatkitPauseForAd"];
}

- (void)aatResumeAfterAdWithPlacement:(id <AATPlacement> _Nonnull)placement {
    [self fireEventWithPlacementName:self.placementName eventName:@"aatkitResumeAfterAd"];
}

- (void)aatHaveAdWithPlacement:(id <AATPlacement> _Nonnull)placement {
    [self fireEventWithPlacementName:self.placementName eventName:@"aatkitHaveAd"];
}

- (void)aatNoAdWithPlacement:(id <AATPlacement> _Nonnull)placement {
    [self fireEventWithPlacementName:self.placementName eventName:@"aatkitNoAd"];
}

- (void)aatUserEarnedIncentiveWithPlacement:(id <AATPlacement> _Nonnull)placement aatReward:(AATReward * _Nonnull)aatReward {
    [self fireIncentiveEvent:self.placementName reward:aatReward];
}

- (void)fireEventWithPlacementName:(NSString*)placementName eventName:(NSString*) eventName {
    NSString* data = [NSString stringWithFormat:@"{placementName:'%@'}", placementName];
    NSString* event = [NSString stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@',%@);", eventName, data];
    [self.cordovaPlugin.commandDelegate evalJs:event];
}

- (void) fireIncentiveEvent:(NSString *)placementName reward:(AATReward * _Nullable)reward {
    NSString* data = nil;
    if(reward != nil) {
        data = [NSString stringWithFormat:@"{placementName:'%@', reward: { name: '%@', value: '%@' }}", placementName, reward.name, reward.value];
    } else {
        data = [NSString stringWithFormat:@"{placementName:'%@'}", placementName];
    }
    NSString* event = [NSString stringWithFormat:@"javascript:cordova.fireDocumentEvent('aatkitUserEarnedIncentive',%@);", data];
    [self.cordovaPlugin.commandDelegate evalJs:event];
}


@end
