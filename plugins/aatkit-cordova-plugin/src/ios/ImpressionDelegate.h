#import <AATKit/AATKit.h>
#import <Cordova/CDV.h>

@interface ImpressionDelegate : NSObject <AATImpressionDelegate>

@property (nonatomic, weak) CDVPlugin* cordovaPlugin;
@property (nonatomic, retain) NSString* placementName;

-(id)initWithPlacementName:(NSString*)placementName andCordovaPlguin:(CDVPlugin*)cordovaPlugin;

@end
