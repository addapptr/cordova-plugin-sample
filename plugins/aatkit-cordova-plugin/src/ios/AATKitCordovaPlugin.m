#import "AATKitCordovaPlugin.h"
#import <UIKit/UIKit.h>
#import <objc/runtime.h>
#import "StatisticsDelegate.h"
#import "ImpressionDelegate.h"
#import "AATKitDelegate.h"
#import <AATGoogleCMPAdapter/AATGoogleCMPAdapter.h>
#import <AATOguryCMPAdapter/AATOguryCMPAdapter.h>
#import <AATSourcePointCMPAdapter/AATSourcePointCMPAdapter-Swift.h>

@implementation AATKitCordovaPlugin

static NSString* VERSION = @"2.4.0";
static bool initialized = false;
AATConsentImplementation* consentImplementation;
AATManagedConsent* managedConsent;
AATManagedConsent* managedConsentNeedingUI;
NonIABConsent consentForAddapptr;
NSMutableArray<NSNumber*> *vendorConsentObtainedNetworks;
static NSMutableDictionary<NSString*, id<AATStickyBannerPlacement>> *stickyBannerPlacements;
static NSMutableDictionary<NSString*, id<AATFullscreenPlacement>> *fullscreenPlacements;
static NSMutableDictionary<NSString*, id<AATRewardedVideoPlacement>> *rewardedVideoPlacements;
static NSMutableDictionary<NSString*, id<AATAppOpenAdPlacement>> *appOpenPlacements;
static NSMutableArray<NSNumber*> *disbledNetworks;
static NSMutableArray<StatisticsDelegate*> *statisticsDelegates;
static NSMutableArray<ImpressionDelegate*> *impressionDelegates;
static NSMutableArray<AATKitDelegate*> *aatkitDelegates;
static bool debugLog = false;

- (void) pluginLog:(NSString*) message {
    [self pluginLogWithForce:false message:message];
}

- (void) pluginLogWithForce:(BOOL) force message:(NSString*) message {
    if(force || debugLog) {
        NSLog(@"%@ %@", @"[AATKitCordovaPlugin]", message);
    }
}

- (BOOL) isAATKitInitialized {
    if(initialized) {
        return true;
    } else {
        [self pluginLog:@"AATKit is not initialized."];
        return false;
    }
}

- (void) pluginInitialize {
    if(stickyBannerPlacements == nil)
    {
        stickyBannerPlacements = [[NSMutableDictionary alloc] init];
    }
    
    if(fullscreenPlacements == nil)
    {
        fullscreenPlacements = [[NSMutableDictionary alloc] init];
    }
    
    if(rewardedVideoPlacements == nil)
    {
        rewardedVideoPlacements = [[NSMutableDictionary alloc] init];
    }
    
    if(appOpenPlacements == nil)
    {
        appOpenPlacements = [[NSMutableDictionary alloc] init];
    }
    
    if(disbledNetworks == nil)
    {
        disbledNetworks = [[NSMutableArray alloc] init];
    }
    
    if(vendorConsentObtainedNetworks == nil)
    {
        vendorConsentObtainedNetworks = [[NSMutableArray alloc] init];
    }
    
    if(statisticsDelegates == nil) {
        statisticsDelegates = [[NSMutableArray alloc] init];
    }
    
    if(impressionDelegates == nil) {
        impressionDelegates = [[NSMutableArray alloc] init];
    }
    
    if(aatkitDelegates == nil) {
        aatkitDelegates = [[NSMutableArray alloc] init];
    }
}

- (void) initWithConfiguration:(CDVInvokedUrlCommand*)command
{
    NSDictionary* initConfig = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"initWithConfiguration initConfig: %@", initConfig]];
    
    if(initialized) {
        return;
    }
    
    AATConfiguration* configuration = [[AATConfiguration alloc] init];
    configuration.alternativeBundleId = initConfig[@"alternativeBundleId"] ? initConfig[@"alternativeBundleId"] : nil;
    configuration.shouldReportUsingAlternativeBundleId = initConfig[@"shouldReportMetricsUsingAlternativeBundleId"] ? [initConfig[@"shouldReportMetricsUsingAlternativeBundleId"] boolValue] : YES;
    configuration.initialRules = initConfig[@"initialRules"] ? [NSJSONSerialization JSONObjectWithData: initConfig[@"initialRules"] options:NSJSONReadingMutableContainers error:nil] : nil;
    configuration.delegate = self;
    configuration.shouldCacheRules = initConfig[@"shouldCacheRules"] ? [initConfig[@"shouldCacheRules"] boolValue] : YES;
    configuration.testModeAccountId = initConfig[@"testModeAccountId"] ? initConfig[@"testModeAccountId"] : nil;
    bool shouldSkipRules = initConfig[@"shouldSkipRules"] ? [initConfig[@"shouldSkipRules"] boolValue] : NO;
    [configuration setShouldSkipRules:shouldSkipRules];
    configuration.adNetworksOptions = [self createNetworkOptions:initConfig];
    
    [self setRuntimeConfiguration:configuration config:initConfig];
    
    [AATPluginVersioningTool appendPluginInformation:AATPluginNameCordova patchLevel:VERSION];
    
    [AATSDK initAATKitWith:configuration];
    [AATSDK controllerViewDidAppearWithController:self.viewController];
    
    initialized = true;
}

- (AATAdNetworksOptions*)createNetworkOptions:(NSDictionary *)initConfig {
    NSDictionary *networkOptions = initConfig[@"networkOptions"];
    if (networkOptions == nil) {
        return nil;
    }

    AATAppNexusOptions * actualAppNexusOptions = [self createAppNexusOptions:networkOptions];
    AATFeedAdOptions * actualFeedAdOptions = [self createFeedAdOptions:networkOptions];
    AATPubNativeOptions * actualPubNativeOptions = [self createPubNativeOptions:networkOptions];
    AATAdMobOptions * actualAdMobOptions = [self createAdMobOptions:networkOptions];
    AATDFPOptions * actualDfpOptions = [self createDfpOptions:networkOptions];
    AATDatonomyOptions * actualDatanomyOptions = [self createDatanomyOptions:networkOptions];

    return [[AATAdNetworksOptions alloc] initWithAppNexusOptions:actualAppNexusOptions feedAdOptions:actualFeedAdOptions pubNativeOptions:actualPubNativeOptions adMobOptions:actualAdMobOptions dfpOptions:actualDfpOptions datonomyOptions:actualDatanomyOptions];
}

- (AATAppNexusOptions *)createAppNexusOptions:(NSDictionary *)networkOptions {
    NSDictionary *appNexusOptions = networkOptions[@"appNexusOptions"];
    if (appNexusOptions == nil) {
        return nil;
    }

    NSNumber *autoCloseTime = appNexusOptions[@"autoCloseTime"];
    NSNumber *supportNativeBanner = appNexusOptions[@"supportNativeBanner"];
    NSNumber *supportVideoBanner = appNexusOptions[@"supportVideoBanner"];
    BOOL supportNativeBannerBool = supportVideoBanner != nil ? [supportVideoBanner boolValue] : false;
    return [[AATAppNexusOptions alloc] initWithAutoCloseTime:autoCloseTime supportNativeBanner:supportNativeBannerBool];
}

- (AATFeedAdOptions *)createFeedAdOptions:(NSDictionary *)networkOptions {
    NSDictionary *feedAdOptions = networkOptions[@"feedAdOptions"];
    if (feedAdOptions == nil) {
        return nil;
    }

    NSNumber *shutterColor = feedAdOptions[@"shutterColor"];
    NSNumber *disableSpinner = feedAdOptions[@"disableSpinner"];
    BOOL disableSpinnerBool = disableSpinner != nil ? [disableSpinner boolValue] : false;
    return [[AATFeedAdOptions alloc] initWithShutterColor:[self convertNumberToColor:shutterColor] disableSpinner:disableSpinner];
}

- (AATPubNativeOptions *)createPubNativeOptions:(NSDictionary *)networkOptions {
    NSDictionary *pubNativeOptions = networkOptions[@"pubNativeOptions"];
    if (pubNativeOptions == nil) {
        return nil;
    }
    
    NSNumber *skipOffsetForHTMLInterstitial = pubNativeOptions[@"skipOffsetForHTMLInterstitial"];
    NSNumber *skipOffsetForVideoInterstitial = pubNativeOptions[@"skipOffsetForVideoInterstitial"];
    return [[AATPubNativeOptions alloc] initWithSkipOffsetForHTMLInterstitial:skipOffsetForHTMLInterstitial skipOffsetForVideoInterstitial:skipOffsetForVideoInterstitial];
}

- (AATAdMobOptions *)createAdMobOptions:(NSDictionary *)networkOptions {
    NSDictionary *adMobOptions = networkOptions[@"adMobOptions"];
    if (adMobOptions == nil) {
        return nil;
    }
    
    NSNumber *inlineBannerMaxHeight = adMobOptions[@"inlineBannerMaxHeight"];
    if(inlineBannerMaxHeight == nil) {
        return nil;
    }
    return [[AATAdMobOptions alloc] initWithInlineBannerMaxHeight:[inlineBannerMaxHeight intValue]];
}

- (AATDFPOptions *)createDfpOptions:(NSDictionary *)networkOptions {
    NSDictionary *dfpOptions = networkOptions[@"dfpOptions"];
    if (dfpOptions == nil) {
        return nil;
    }

    NSNumber *inlineBannerMaxHeight = dfpOptions[@"inlineBannerMaxHeight"];
    if(inlineBannerMaxHeight == nil) {
        return nil;
    }
    return [[AATDFPOptions alloc] initWithInlineBannerMaxHeight:[inlineBannerMaxHeight intValue]];
}

- (AATDatonomyOptions *)createDatanomyOptions:(NSDictionary *)networkOptions {
    NSDictionary *datonomyOptions = networkOptions[@"datonomyOptions"];
    if (datonomyOptions == nil) {
        return nil;
    }

    NSString *datonomyAPIKey = datonomyOptions[@"datonomyAPIKey"];
    return [[AATDatonomyOptions alloc] initWithDatonomyAPIKey:datonomyAPIKey];
}

- (UIColor*)convertNumberToColor:(NSNumber *)colorNumber {
    if(colorNumber == nil) {
        return nil;
    }

    int rgb = [colorNumber intValue];
    CGFloat red = ((rgb >> 16) & 0xFF) / 255.0;
    CGFloat green = ((rgb >> 8) & 0xFF) / 255.0;
    CGFloat blue = (rgb & 0xFF) / 255.0;

    return [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
}

- (void) reconfigure:(CDVInvokedUrlCommand *)command {
    NSDictionary* config = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"reconfigure config: %@", config]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    AATRuntimeConfiguration* configuration = [[AATRuntimeConfiguration alloc] init];
    [self setRuntimeConfiguration:configuration config:config];
    [AATSDK reconfigureWithConfiguration:configuration];
}

- (void)setRuntimeConfiguration:(AATRuntimeConfiguration *)configuration config:(NSDictionary *)config {
    configuration.isUseGeoLocation = config[@"useGeoLocation"] ? [config[@"useGeoLocation"] boolValue] : NO;
    configuration.consentRequired = config[@"consentRequired"] ? [config[@"consentRequired"] boolValue] : NO;
    
    if(config[@"consent"]) {
        NSDictionary* consent = config[@"consent"];
        
        if([consent[@"type"] caseInsensitiveCompare:@"simpleConsent"] == NSOrderedSame) {
            NSString* nonIABConsent = consent[@"nonIABConsent"];
            NonIABConsent nativeNonIABConsent = [self getNonIABConsent:nonIABConsent];
            consentImplementation = [[AATSimpleConsent alloc] initWithNonIABConsent:nativeNonIABConsent];
            configuration.consent = consentImplementation;
        } else if([consent[@"type"] caseInsensitiveCompare:@"managedCMPGoogle"] == NSOrderedSame) {
            AATCMPGoogle* cmp = [[AATCMPGoogle alloc] init];
            AATShowIfNeededSetting showIfNeededSetting = getShowIfNeededSetting(consent);
            managedConsent = [[AATManagedConsent alloc] initWithCmp:cmp delegate:self showIfNeededSetting:showIfNeededSetting];
            consentImplementation = managedConsent;
            configuration.consent = managedConsent;
        } else if([consent[@"type"] caseInsensitiveCompare:@"managedCMPOgury"] == NSOrderedSame) {
            NSString* assetKey = consent[@"assetKeyCMPOgury"];
            AATShowIfNeededSetting showIfNeededSetting = getShowIfNeededSetting(consent);
            AATCMPOgury* cmp = [[AATCMPOgury alloc] initWith:assetKey];
            managedConsent = [[AATManagedConsent alloc] initWithCmp:cmp delegate:self showIfNeededSetting:showIfNeededSetting];
            consentImplementation = managedConsent;
            configuration.consent = managedConsent;
        } else if([consent[@"type"] caseInsensitiveCompare:@"managedCMPSourcePoint"] == NSOrderedSame) {
            NSNumber* accountId = consent[@"yourAccountID"];
            NSNumber* propertyId = consent[@"yourPropertyId"];
            NSString* propertyName = consent[@"yourPropertyName"];
            NSString* pmId = consent[@"yourPMId"];
            AATShowIfNeededSetting showIfNeededSetting = getShowIfNeededSetting(consent);
            AATCMPSourcepoint* cmp = [[AATCMPSourcepoint alloc] initWithAccountId:[accountId stringValue] propertyId:[propertyId stringValue] propertyName:propertyName pmId:pmId];
            managedConsent = [[AATManagedConsent alloc] initWithCmp:cmp delegate:self showIfNeededSetting:showIfNeededSetting];
            consentImplementation = managedConsent;
            configuration.consent = managedConsent;
        } else if([consent[@"type"] caseInsensitiveCompare:@"vendor"] == NSOrderedSame) {
            NSString* nonIABConsentForAddapptr = consent[@"consentForAddapptr"];
            consentForAddapptr = [self getNonIABConsent:nonIABConsentForAddapptr];
            
            [vendorConsentObtainedNetworks removeAllObjects];
            NSArray* obtainedNetworks = consent[@"vendorConsentObtainedNetworks"];
            for(int i=0; i<obtainedNetworks.count; i++) {
                NSString* networkName = obtainedNetworks[i];
                AATAdNetwork network = [self convertStringToNetwork:networkName];
                [vendorConsentObtainedNetworks addObject:[NSNumber numberWithInt:network]];
            }
            
            consentImplementation = [[AATVendorConsent alloc] initWithDelegate:self];
            configuration.consent = consentImplementation;
        }
        
        [self setNoConsentNetworkStopSetIfNeeded:consent];
    }
}

AATShowIfNeededSetting getShowIfNeededSetting(NSDictionary* consent) {
    AATShowIfNeededSetting setting = AATShowIfNeededSettingServerSideControl;
    
    if(consent[@"showIfNeededSetting"] == nil) {
        return setting;
    }
    
    NSString* showIfNeededSetting = consent[@"showIfNeededSetting"];
    showIfNeededSetting = [showIfNeededSetting uppercaseString];
    
    if([showIfNeededSetting caseInsensitiveCompare:@"always"] == NSOrderedSame) {
        setting = AATShowIfNeededSettingAlways;
    }
    else if([showIfNeededSetting caseInsensitiveCompare:@"never"] == NSOrderedSame) {
        setting = AATShowIfNeededSettingNever;
    }
    else if([showIfNeededSetting caseInsensitiveCompare:@"serverSideControl"] == NSOrderedSame) {
        setting = AATShowIfNeededSettingServerSideControl;
    }
    
    return setting;
}

- (void)setNoConsentNetworkStopSetIfNeeded:(NSDictionary *)consent {
    if(consentImplementation != nil && consent[@"noConsentNetworkStopSet"]) {
        NSArray* networks = consent[@"noConsentNetworkStopSet"];
        NSMutableSet<NSNumber*>* noConsentNetworkStopSet = [[NSMutableSet alloc] init];
        
        for(int i=0; i<networks.count; i++) {
            NSString* networkName = networks[i];
            AATAdNetwork network = [self convertStringToNetwork:networkName];
            [noConsentNetworkStopSet addObject:[NSNumber numberWithInt:network]];
        }
        
        if(noConsentNetworkStopSet.count > 0) {
            [consentImplementation setNoConsentNetworkStopSet:noConsentNetworkStopSet];
        }
    }
}

- (NonIABConsent)getNonIABConsent:(NSString *)nonIABConsent {
    NonIABConsent nativeNonIABConsent;
    
    if([nonIABConsent caseInsensitiveCompare:@"obtained"] == NSOrderedSame) {
        nativeNonIABConsent = NonIABConsentObtained;
    } else if([nonIABConsent caseInsensitiveCompare:@"withheld"] == NSOrderedSame) {
        nativeNonIABConsent = NonIABConsentWithheld;
    } else {
        nativeNonIABConsent = NonIABConsentUnknown;
    }
    return nativeNonIABConsent;
}

- (void) getVersion:(CDVInvokedUrlCommand*)command {
    [self pluginLog:@"getVersion"];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:VERSION];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) setDebugEnabled:(CDVInvokedUrlCommand*)command {
    BOOL enabled = (BOOL) [command.arguments objectAtIndex:0];
    [self pluginLogWithForce:true message: [NSString stringWithFormat:@"setDebugEnabled enabled: %d", enabled]];
    
    [AATSDK setLogLevelWithLogLevel:AATLogLevelVerbose];
    debugLog = true;
}

- (void) setDebugShakeEnabled:(CDVInvokedUrlCommand*)command {
    BOOL enabled = (BOOL) [command.arguments objectAtIndex:0];
    [self pluginLog: [NSString stringWithFormat:@"setDebugShakeEnabled enabled: %d", enabled]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    if(enabled) {
        [AATSDK enableDebugScreen];
    }
    else {
        [AATSDK disableDebugScreen];
    }
}

- (void) setIsChildDirected:(CDVInvokedUrlCommand*)command {
    BOOL enabled = (BOOL) [command.arguments objectAtIndex:0];
    [self pluginLog: [NSString stringWithFormat:@"setIsChildDirected enabled: %d", enabled]];
    
    [AATSDK setIsChildDirected:enabled];
}

- (void) getDebugInfo:(CDVInvokedUrlCommand*)command {
    [self pluginLog: @"getDebugInfo"];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    NSArray* debugInfo = @[[AATSDK getDebugInfo]];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:debugInfo];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) configureDebugScreen:(CDVInvokedUrlCommand*)command {
    NSDictionary* configuration = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"configureDebugScreen configuration: %@", configuration]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    NSString *title = configuration[@"title"] ? configuration[@"title"] : @"";
    BOOL showBundleId = configuration[@"showBundleId"] ? [configuration[@"showBundleId"] boolValue] : YES;
    BOOL showTestMode = configuration[@"showTestMode"] ? [configuration[@"showTestMode"] boolValue] : YES;
    BOOL showLoadedAndLoadingAds = configuration[@"showLoadedAndLoadingAds"] ? [configuration[@"showLoadedAndLoadingAds"] boolValue] : YES;
    BOOL showAvailableNetworks = configuration[@"showAvailableNetworks"] ? [configuration[@"showAvailableNetworks"] boolValue] : YES;
    BOOL showDisabledNetworks = configuration[@"showDisabledNetworks"] ? [configuration[@"showDisabledNetworks"] boolValue] : YES;
    BOOL showRemovedNetworkSDKs = configuration[@"showRemovedNetworkSDKs"] ? [configuration[@"showRemovedNetworkSDKs"] boolValue] : YES;
    BOOL showUnsupportedNetworks = configuration[@"showUnsupportedNetworks"] ? [configuration[@"showUnsupportedNetworks"] boolValue] : YES;
    BOOL showExtraSDKs = configuration[@"showExtraSDKs"] ? [configuration[@"showExtraSDKs"] boolValue] : YES;
    BOOL showConsent = configuration[@"showConsent"] ? [configuration[@"showConsent"] boolValue] : YES;
    BOOL showAdvertisingId = configuration[@"showAdvertisingId"] ? [configuration[@"showAdvertisingId"] boolValue] : YES;
    BOOL showDeviceType = configuration[@"showDeviceType"] ? [configuration[@"showDeviceType"] boolValue] : YES;
    
    AATDebugScreenConfiguration* debugScreenConfiguration = [[AATDebugScreenConfiguration alloc] initWithAppLogo:nil title:title showBundleId:showBundleId showTestMode:showTestMode showLoadedAndLoadingAds:showLoadedAndLoadingAds showDisabledNetworks:showDisabledNetworks showRemovedNetworkSDKs:showRemovedNetworkSDKs showDeviceType:showDeviceType showExtraSDKs:showExtraSDKs showConsent:showConsent showIDFA:showAdvertisingId];
    [AATSDK configureDebugScreenWithConfiguration:debugScreenConfiguration];
}

- (void) showConsentDialogIfNeeded:(CDVInvokedUrlCommand*)command {
    [self pluginLog:@"showConsentDialogIfNeeded"];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    if(managedConsentNeedingUI != nil)
    {
        [managedConsentNeedingUI showIfNeeded:self.viewController];
        managedConsentNeedingUI = nil;
    } else if(managedConsent != nil) {
        [managedConsent showIfNeeded:self.viewController];
    }
}

- (void) editConsent:(CDVInvokedUrlCommand*)command {
    [self pluginLog:@"editConsnet"];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    if(managedConsent != nil) {
        [managedConsent editConsent:self.viewController];
    }
}

- (void) reloadConsent:(CDVInvokedUrlCommand*)command {
    [self pluginLog:@"reloadConsnet"];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    if(managedConsent != nil) {
        [managedConsent reload:self.viewController];
    }
}

- (void) setNetworkEnabled:(CDVInvokedUrlCommand*)command {
    NSString* networkName = [command.arguments objectAtIndex:0];
    BOOL enabled = (BOOL) [command.arguments objectAtIndex:1];
    [self pluginLog:[NSString stringWithFormat:@"setNetworkEnabled networkName: %@ enabled: %d", networkName, enabled]];
    
    if(![self isAATKitInitialized]) {
        return;
    }

    AATAdNetwork adNetwork = [self convertStringToNetwork:networkName];

    if(adNetwork != -1) {
        if(enabled) {
            if ([disbledNetworks containsObject:[NSNumber numberWithInt:adNetwork]]) {
                [disbledNetworks removeObject:[NSNumber numberWithInt:adNetwork]];
            }
            
            [AATSDK setNetworkEnabledWithNetwork:adNetwork enabled:enabled];
        } else {
            if (![disbledNetworks containsObject:[NSNumber numberWithInt:adNetwork]]) {
                [disbledNetworks addObject:[NSNumber numberWithInt:adNetwork]];
            }
            
            [AATSDK setNetworkEnabledWithNetwork:adNetwork enabled:enabled];
        }
    }
}

- (void) isNetworkEnabled:(CDVInvokedUrlCommand*)command {
    NSString* networkName = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"isNetworkEnabled networkName: %@", networkName]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    AATAdNetwork adNetwork = [self convertStringToNetwork:networkName];
    
    if (adNetwork != -1) {
        BOOL enabled = true;
        
        if ([disbledNetworks containsObject:[NSNumber numberWithInt:adNetwork]]) {
            enabled = false;
        }
        
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:enabled];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }
}

- (void) setTargetingInfo:(CDVInvokedUrlCommand*)command {
    NSDictionary* targetingInfo = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"setTargetingInfo targetingInfo: %@", targetingInfo]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    [AATSDK setTargetingInfoWithInfo:targetingInfo];
}

- (void) setTargetingInfoForPlacement:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    NSDictionary* targetingInfo = [command.arguments objectAtIndex:1];
    [self pluginLog:[NSString stringWithFormat:@"setTargetingInfo placementName: %@ targetingInfo: %@", placementName, targetingInfo]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        [bannerPlacement setTargetingInfo:targetingInfo];
    }
    id<AATFullscreenPlacement> fullscreenPlacement = fullscreenPlacements[placementName];
    if (fullscreenPlacement != nil) {
        [fullscreenPlacement setTargetingInfo:targetingInfo];
    }
    id<AATRewardedVideoPlacement> rewardedPlacement = rewardedVideoPlacements[placementName];
    if (rewardedPlacement != nil) {
        [rewardedPlacement setTargetingInfo:targetingInfo];
    }
    id<AATAppOpenAdPlacement> appOpenPlacement = appOpenPlacements[placementName];
    if (appOpenPlacement != nil) {
        [appOpenPlacement setTargetingInfo:targetingInfo];
    }
}

- (void) setContentTargetingUrl:(CDVInvokedUrlCommand*)command {
    NSString* url = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"setTargetingInfo url: %@", url]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    [AATSDK setContentTargetingUrlWithTargetingUrl:url];
}

- (void) setContentTargetingUrlForPlacement:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    NSString* url = [command.arguments objectAtIndex:1];
    [self pluginLog:[NSString stringWithFormat:@"setTargetingInfo placementName: %@ url: %@", placementName, url]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        [bannerPlacement setContentTargetingUrl:url];
    }
    id<AATFullscreenPlacement> fullscreenPlacement = fullscreenPlacements[placementName];
    if (fullscreenPlacement != nil) {
        [fullscreenPlacement setContentTargetingUrl:url];
    }
    id<AATRewardedVideoPlacement> rewardedPlacement = rewardedVideoPlacements[placementName];
    if (rewardedPlacement != nil) {
        [rewardedPlacement setContentTargetingUrl:url];
    }
    id<AATAppOpenAdPlacement> appOpenPlacement = appOpenPlacements[placementName];
    if (appOpenPlacement != nil) {
        [appOpenPlacement setContentTargetingUrl:url];
    }
}

- (void) addAdNetworkForKeywordTargeting:(CDVInvokedUrlCommand*)command {
    NSString* networkName = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"addAdNetworkForKeywordTargeting networkName: %@", networkName]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    AATAdNetwork adNetwork = [self convertStringToNetwork:networkName];

    if(adNetwork != -1) {
        [AATSDK addAdNetworkForKeywordTargetingWithNetwork:adNetwork];
    }
}

- (void) removeAdNetworkForKeywordTargeting:(CDVInvokedUrlCommand*)command {
    NSString* networkName = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"removeAdNetworkForKeywordTargeting networkName: %@", networkName]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    AATAdNetwork adNetwork = [self convertStringToNetwork:networkName];

    if(adNetwork != -1) {
        [AATSDK removeAdNetworkForKeywordTargetingWithNetwork:adNetwork];
    }
}

- (AATAdNetwork)convertStringToNetwork:(NSString *)networkName {
    networkName = [networkName uppercaseString];
    AATAdNetwork adNetwork = -1;
    
    if([networkName isEqualToString:@"ADCOLONY"]) {
        adNetwork = AATAdNetworkADCOLONY;
    }
    else if([networkName isEqualToString:@"ADMOB"]) {
        adNetwork = AATAdNetworkADMOB;
    }
    else if([networkName isEqualToString:@"RTB2"]) {
        adNetwork = AATAdNetworkRTB2;
    }
    else if([networkName isEqualToString:@"AMAZONHB"]) {
        adNetwork = AATAdNetworkAMAZONHB;
    }
    else if([networkName isEqualToString:@"APPLOVIN"]) {
        adNetwork = AATAdNetworkAPPLOVIN;
    }
    else if([networkName isEqualToString:@"APPLOVINMAX"]) {
        adNetwork = AATAdNetworkAPPLOVINMAX;
    }
    else if([networkName isEqualToString:@"APPNEXUS"]) {
        adNetwork = AATAdNetworkAPPNEXUS;
    }
    else if([networkName isEqualToString:@"BLUESTACK"]) {
        adNetwork = AATAdNetworkBLUESTACK;
    }
    else if([networkName isEqualToString:@"CRITEOSDK"]) {
        adNetwork = AATAdNetworkCRITEOSDK;
    }
    else if([networkName isEqualToString:@"DFP"]) {
        adNetwork = AATAdNetworkDFP;
    }
    else if([networkName isEqualToString:@"DFPDIRECT"]) {
        adNetwork = AATAdNetworkDFPDIRECT;
    }
    else if([networkName isEqualToString:@"FACEBOOK"]) {
        adNetwork = AATAdNetworkFACEBOOK;
    }
    else if([networkName isEqualToString:@"FEEDAD"]) {
        adNetwork = AATAdNetworkFEEDAD;
    }
    else if([networkName isEqualToString:@"GRAVITERTB"]) {
        adNetwork = AATAdNetworkGRAVITERTB;
    }
    else if([networkName isEqualToString:@"INMOBI"]) {
        adNetwork = AATAdNetworkINMOBI;
    }
    else if([networkName isEqualToString:@"IRONSOURCE"]) {
        adNetwork = AATAdNetworkIRONSOURCE;
    }
    else if([networkName isEqualToString:@"MINTEGRAL"]) {
        adNetwork = AATAdNetworkMINTEGRAL;
    }
    else if([networkName isEqualToString:@"OGURY"]) {
        adNetwork = AATAdNetworkOGURY;
    }
    else if([networkName isEqualToString:@"PUBNATIVE"]) {
        adNetwork = AATAdNetworkPUBNATIVE;
    }
    else if([networkName isEqualToString:@"RUBICON"]) {
        adNetwork = AATAdNetworkRUBICON;
    }
    else if([networkName isEqualToString:@"SMAATO"]) {
        adNetwork = AATAdNetworkSMAATO;
    }
    else if([networkName isEqualToString:@"SMARTAD"]) {
        adNetwork = AATAdNetworkSMARTAD;
    }
    else if([networkName isEqualToString:@"SMARTADSERVERDIRECT"]) {
        adNetwork = AATAdNetworkSMARTADSERVERDIRECT;
    }
    else if([networkName isEqualToString:@"TAPPX"]) {
        adNetwork = AATAdNetworkTAPPX;
    }
    else if([networkName isEqualToString:@"TEADS"]) {
        adNetwork = AATAdNetworkTEADS;
    }
    else if([networkName isEqualToString:@"UNITY"]) {
        adNetwork = AATAdNetworkUNITY;
    }
    else if([networkName isEqualToString:@"VUNGLE2"]) {
        adNetwork = AATAdNetworkVUNGLE2;
    }
    else if([networkName isEqualToString:@"YOC"]) {
        adNetwork = AATAdNetworkYOC;
    }
    else {
        [self pluginLog:[NSString stringWithFormat:@"Network %@ is not supported", networkName]];
    }
    
    return adNetwork;
}

- (StatisticsDelegate *)createStatisticsDelegate:(NSString *)placementName {
    StatisticsDelegate* statisticsDelegate = [[StatisticsDelegate alloc] initWithPlacementName:placementName andCordovaPlugin:self];
    [statisticsDelegates addObject:statisticsDelegate];
    return statisticsDelegate;
}

- (void) createPlacement:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    NSString* placementSize = [command.arguments objectAtIndex:1];
    [self pluginLog:[NSString stringWithFormat:@"createPlacement placementName: %@ placementSize: %@", placementName, placementSize]];
    
    if(![self isAATKitInitialized]) {
        return;
    }

    if([placementSize caseInsensitiveCompare:@"Banner320x53"] == NSOrderedSame) {
        [self createStickyBannerPlacement:placementName andSize:AATBannerPlacementSizeBanner320x53];
    } else if([placementSize caseInsensitiveCompare:@"Banner320x50"] == NSOrderedSame) {
        [self createStickyBannerPlacement:placementName andSize:AATBannerPlacementSizeBanner320x50];
    } else if([placementSize caseInsensitiveCompare:@"Banner768x90"] == NSOrderedSame) {
        [self createStickyBannerPlacement:placementName andSize:AATBannerPlacementSizeBanner768x90];
    } else if([placementSize caseInsensitiveCompare:@"Banner300x250"] == NSOrderedSame) {
        [self createStickyBannerPlacement:placementName andSize:AATBannerPlacementSizeBanner300x250];
    } else if([placementSize caseInsensitiveCompare:@"Fullscreen"] == NSOrderedSame) {
        [self createFullscreenPlacement:placementName];
    }
}

- (void)createStickyBannerPlacement:(NSString*)placementName andSize:(AATBannerPlacementSize) size {
    StatisticsDelegate * statisticsDelegate = [self createStatisticsDelegate:placementName];
    id<AATStickyBannerPlacement> placement = [AATSDK createStickyBannerPlacementWithName:placementName size:size];
    [placement setDelegate: [self createPlacementDelegate:placementName]];
    [placement setStatisticsDelegate:statisticsDelegate];
    [stickyBannerPlacements setObject:placement forKey:placementName];
}

- (void)createFullscreenPlacement:(NSString*)placementName {
    StatisticsDelegate * statisticsDelegate = [self createStatisticsDelegate:placementName];
    id<AATFullscreenPlacement> placement = [AATSDK createFullscreenPlacementWithName:placementName];
    [placement setDelegate: [self createPlacementDelegate:placementName]];
    [placement setStatisticsDelegate:statisticsDelegate];
    [fullscreenPlacements setObject:placement forKey:placementName];
}

- (void) createRewardedVideoPlacement:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"createRewardedVideoPlacement placementName: %@", placementName]];
    
    if(![self isAATKitInitialized]) {
        return;
    }

    StatisticsDelegate * statisticsDelegate = [self createStatisticsDelegate:placementName];
    id<AATRewardedVideoPlacement> rewardedVideoPlacement = [AATSDK createRewardedVideoPlacementWithName:placementName];
    [rewardedVideoPlacement setDelegate: [self createPlacementDelegate:placementName]];
    [rewardedVideoPlacement setStatisticsDelegate:statisticsDelegate];
    [rewardedVideoPlacements setObject:rewardedVideoPlacement forKey:placementName];
}

- (void) createAppOpenAdPlacement:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"createAppOpenAdPlacement placementName: %@", placementName]];
    
    if(![self isAATKitInitialized]) {
        return;
    }

    StatisticsDelegate * statisticsDelegate = [self createStatisticsDelegate:placementName];
    id<AATAppOpenAdPlacement> appOpenPlacement = [AATSDK createAppOpenAdPlacementWithPlacementName:placementName];
    [appOpenPlacement setDelegate: [self createPlacementDelegate:placementName]];
    [appOpenPlacement setStatisticsDelegate:statisticsDelegate];
    [appOpenPlacements setObject:appOpenPlacement forKey:placementName];
}

- (void) setCollapsibleBannerOptions:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    NSDictionary* collapsibleBannerOptions = [command.arguments objectAtIndex:1];
    [self pluginLog:[NSString stringWithFormat:@"setCollapsibleBannerOptions placementName: %@ options:%@", collapsibleBannerOptions]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    NSNumber* minDelayInSeconds = collapsibleBannerOptions[@"minDelayInSeconds"];
    NSString* position = collapsibleBannerOptions[@"position"] != nil ? collapsibleBannerOptions[@"position"]: @"TOP";
    position = [position uppercaseString];
    AATCollapsibleBannerPosition actualPosition = AATCollapsibleBannerPositionTop;
    if ([position isEqualToString:@"BOTTOM"]) {
        actualPosition = AATCollapsibleBannerPositionBottom;
    }
    AATCollapsibleBannerOptions* options = [[AATCollapsibleBannerOptions alloc] initWithPosition:actualPosition minDelay:minDelayInSeconds != nil ? [minDelayInSeconds intValue] : 0];
    
    id<AATStickyBannerPlacement> sttickyBannerPlacement = stickyBannerPlacements[placementName];
    if (sttickyBannerPlacement != nil) {
        [sttickyBannerPlacement setCollapsableBannerOptions:options];
    }
}

- (AATKitDelegate*)createPlacementDelegate:(NSString*)placementName {
    AATKitDelegate* delegate = [[AATKitDelegate alloc] initWithPlacementName:placementName andCordovaPlguin:self];
    [aatkitDelegates addObject:delegate];
    return delegate;
}

- (void) setImpressionListener:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"setImpressionListener placementName: %@", placementName]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        ImpressionDelegate* impressionDelegate = [[ImpressionDelegate alloc] initWithPlacementName:placementName andCordovaPlguin:self];
        [impressionDelegates addObject:impressionDelegate];
        [bannerPlacement setImpressionDelegate:impressionDelegate];
    }
    id<AATFullscreenPlacement> fullscreenPlacement = fullscreenPlacements[placementName];
    if (fullscreenPlacement != nil) {
        ImpressionDelegate* impressionDelegate = [[ImpressionDelegate alloc] initWithPlacementName:placementName andCordovaPlguin:self];
        [impressionDelegates addObject:impressionDelegate];
        [fullscreenPlacement setImpressionDelegate:impressionDelegate];
    }
    id<AATRewardedVideoPlacement> rewardedPlacement = rewardedVideoPlacements[placementName];
    if (rewardedPlacement != nil) {
        ImpressionDelegate* impressionDelegate = [[ImpressionDelegate alloc] initWithPlacementName:placementName andCordovaPlguin:self];
        [impressionDelegates addObject:impressionDelegate];
        [rewardedPlacement setImpressionDelegate:impressionDelegate];
    }
    id<AATAppOpenAdPlacement> appOpenPlacement = appOpenPlacements[placementName];
    if (appOpenPlacement != nil) {
        ImpressionDelegate* impressionDelegate = [[ImpressionDelegate alloc] initWithPlacementName:placementName andCordovaPlguin:self];
        [impressionDelegates addObject:impressionDelegate];
        [appOpenPlacement setImpressionDelegate:impressionDelegate];
    }
}

- (void) setPublisherProvidedId:(CDVInvokedUrlCommand*)command {
    NSString* providedId = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"setImpressionListener providedId: %@", providedId]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    [AATSDK setPublisherProvidedId:providedId];
}

- (void) reloadPlacement:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"reloadPlacement placementName: %@", placementName]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        [bannerPlacement reload];
    }
    id<AATFullscreenPlacement> fullscreenPlacement = fullscreenPlacements[placementName];
    if (fullscreenPlacement != nil) {
        [fullscreenPlacement reload];
    }
    id<AATRewardedVideoPlacement> rewardedPlacement = rewardedVideoPlacements[placementName];
    if (rewardedPlacement != nil) {
        [rewardedPlacement reload];
    }
    id<AATAppOpenAdPlacement> appOpenPlacement = appOpenPlacements[placementName];
    if (appOpenPlacement != nil) {
        [appOpenPlacement reload];
    }
}

- (void) reloadPlacementForced:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    BOOL force = (BOOL) [command.arguments objectAtIndex:1];
    [self pluginLog:[NSString stringWithFormat:@"reloadPlacementForced placementName: %@ force: %d", placementName, force]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        [bannerPlacement reloadWithForceLoad:force];
    }
}

- (void) startPlacementAutoReload:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"startPlacementAutoReload placementName: %@", placementName]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        [bannerPlacement startAutoReload];
    }
    id<AATFullscreenPlacement> fullscreenPlacement = fullscreenPlacements[placementName];
    if (fullscreenPlacement != nil) {
        [fullscreenPlacement startAutoReload];
    }
    id<AATRewardedVideoPlacement> rewardedPlacement = rewardedVideoPlacements[placementName];
    if (rewardedPlacement != nil) {
        [rewardedPlacement startAutoReload];
    }
    id<AATAppOpenAdPlacement> appOpenPlacement = appOpenPlacements[placementName];
    if (appOpenPlacement != nil) {
        [appOpenPlacement startAutoReload];
    }
}

- (void) stopPlacementAutoReload:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"stopPlacementAutoReload placementName: %@", placementName]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        [bannerPlacement stopAutoReload];
    }
    id<AATFullscreenPlacement> fullscreenPlacement = fullscreenPlacements[placementName];
    if (fullscreenPlacement != nil) {
        [fullscreenPlacement stopAutoReload];
    }
    id<AATRewardedVideoPlacement> rewardedPlacement = rewardedVideoPlacements[placementName];
    if (rewardedPlacement != nil) {
        [rewardedPlacement stopAutoReload];
    }
    id<AATAppOpenAdPlacement> appOpenPlacement = appOpenPlacements[placementName];
    if (appOpenPlacement != nil) {
        [appOpenPlacement stopAutoReload];
    }
}

- (void) muteVideoAds:(CDVInvokedUrlCommand*)command {
    bool mute = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"muteVideoAds mute: %d", mute]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    [AATSDK setVideoAdsMuted:mute];
}

- (void) addPlacementToView:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"addPlacementToView placementName: %@", placementName]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        UIView* bannerPlacementView = [bannerPlacement getPlacementView];
        [self.webView addSubview:bannerPlacementView];
    }
}

- (void) removePlacementFromView:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"removePlacementFromView placementName: %@", placementName]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        UIView* bannerPlacementView = [bannerPlacement getPlacementView];
        [bannerPlacementView removeFromSuperview];
        [bannerPlacement stopAutoReload];
    }
}

- (void) setPlacementAlignment:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    NSString* alignment = [command.arguments objectAtIndex:1];
    [self pluginLog:[NSString stringWithFormat:@"setPlacementAlignment placementName: %@ alignment: %@", placementName, alignment]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        UIView* bannerPlacementView = [bannerPlacement getPlacementView];
        [self alignBanner:alignment bannerPlacementView:bannerPlacementView];
    }
}

- (void) setPlacementAlignmentWithOffset:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    NSString* alignment = [command.arguments objectAtIndex:1];
    NSNumber* x = [command.arguments objectAtIndex:2];
    NSNumber* y = [command.arguments objectAtIndex:3];
    [self pluginLog:[NSString stringWithFormat:@"setPlacementAlignmentWithOffset placementName: %@ alignment: %@ x:%d y:%d" , placementName, alignment, [x intValue], [y intValue]]];
     
    if(![self isAATKitInitialized]) {
        return;
    }
     
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        UIView* bannerPlacementView = [bannerPlacement getPlacementView];
        [self alignBanner:alignment bannerPlacementView:bannerPlacementView];
        [self setBannerOffset:bannerPlacementView x:x y:y];
    }
}

- (void)alignBanner:(NSString *)alignment bannerPlacementView:(UIView*)bannerPlacementView {
    CGSize controllerSize = self.viewController.view.bounds.size;
    CGRect frame = bannerPlacementView.frame;
    
    UIEdgeInsets safeAreaInsets = UIEdgeInsetsZero;
    if (@available(iOS 11.0, *)) {
        safeAreaInsets = self.viewController.view.safeAreaInsets;
    }
    
    alignment = [alignment lowercaseString];
    
    if([alignment isEqualToString:@"topleft"]) {
        frame.origin.x = 0 + safeAreaInsets.left;
        frame.origin.y = 0 + safeAreaInsets.top;
    } else if([alignment isEqualToString:@"topcenter"]) {
        frame.origin.x = controllerSize.width/2 - bannerPlacementView.frame.size.width/2;
        frame.origin.y = 0 + safeAreaInsets.top;
    } else if([alignment isEqualToString:@"topright"]) {
        frame.origin.x = controllerSize.width - bannerPlacementView.frame.size.width - safeAreaInsets.right;
        frame.origin.y = 0 + safeAreaInsets.top;
    } else if([alignment isEqualToString:@"bottomleft"]) {
        frame.origin.x = 0 + safeAreaInsets.left;
        frame.origin.y = controllerSize.height - bannerPlacementView.frame.size.height - safeAreaInsets.bottom;
    } else if([alignment isEqualToString:@"bottomcenter"]) {
        frame.origin.x = controllerSize.width/2 - bannerPlacementView.frame.size.width/2;
        frame.origin.y = controllerSize.height - bannerPlacementView.frame.size.height - safeAreaInsets.bottom;
    } else if([alignment isEqualToString:@"bottomright"]) {
        frame.origin.x = controllerSize.width - bannerPlacementView.frame.size.width - safeAreaInsets.right;
        frame.origin.y = controllerSize.height - bannerPlacementView.frame.size.height - safeAreaInsets.bottom;
    } else {
        [self pluginLog:[NSString stringWithFormat:@"Cannot align banner placement, %@ is wrong alignment value.", alignment]];
    }
    
    bannerPlacementView.frame = frame;
}

- (void)setBannerOffset:(UIView *)bannerPlacementView x:(NSNumber *)x y:(NSNumber *)y {
    CGRect frame = bannerPlacementView.frame;
    frame.origin.x += [x intValue];
    frame.origin.y += [y intValue];
    bannerPlacementView.frame = frame;
}

- (void) setPlacementPosition:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    NSNumber* x = [command.arguments objectAtIndex:1];
    NSNumber* y = [command.arguments objectAtIndex:2];

    [self pluginLog:[NSString stringWithFormat:@"setPlacementPosition placementName: %@ x:%d y:%d", placementName, [x intValue], [y intValue]]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        UIView* bannerPlacementView = [bannerPlacement getPlacementView];
        CGRect frame = bannerPlacementView.frame;
        frame.origin.x = [x intValue];
        frame.origin.y = [y intValue];
        bannerPlacementView.frame = frame;
    }
}

- (void) setPlacementContentGravity:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    NSString* gravity = [command.arguments objectAtIndex:1];
    [self pluginLog:[NSString stringWithFormat:@"setPlacementContentGravity placementName: %@ gravity: %@", placementName, gravity]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    gravity = [gravity lowercaseString];
    AATBannerAlign* alignment = nil;
    
    if([gravity isEqualToString:@"top"]) {
        alignment = [[AATBannerAlign alloc] initWithHorizontalAlign:HorizontalAlignCenter verticalAlign:VerticalAlignTop];
    } else if([gravity isEqualToString:@"bottom"]) {
        alignment = [[AATBannerAlign alloc] initWithHorizontalAlign:HorizontalAlignCenter verticalAlign:VerticalAlignBottom];
    } else if([gravity isEqualToString:@"center"]) {
        alignment = [[AATBannerAlign alloc] initWithHorizontalAlign:HorizontalAlignCenter verticalAlign:VerticalAlignCenter];
    } else {
        [self pluginLog:[NSString stringWithFormat:@"Cannot set content gravity for placement, %@ is wrong gravity value.", gravity]];
    }
    
    id<AATStickyBannerPlacement> bannerPlacement = stickyBannerPlacements[placementName];
    if (bannerPlacement != nil) {
        [bannerPlacement setBannerAlignWithAlignment:alignment];
    }
}

- (void) showPlacement:(CDVInvokedUrlCommand*)command {
    NSString* placementName = [command.arguments objectAtIndex:0];
    [self pluginLog:[NSString stringWithFormat:@"showPlacement placementName: %@", placementName]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    bool shown = false;
    
    id<AATFullscreenPlacement> fullscreenPlacement = fullscreenPlacements[placementName];
    if (fullscreenPlacement != nil) {
        [fullscreenPlacement show];
    }
    id<AATRewardedVideoPlacement> rewardedPlacement = rewardedVideoPlacements[placementName];
    if (rewardedPlacement != nil) {
        [rewardedPlacement show];
    }
    id<AATAppOpenAdPlacement> appOpenPlacement = appOpenPlacements[placementName];
    if (appOpenPlacement != nil) {
        [appOpenPlacement show];
    }
    
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:shown];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) isTablet:(CDVInvokedUrlCommand*)command {
    [self pluginLog:[NSString stringWithFormat:@"isTablet"]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    bool isTablet = [AATSDK isTablet];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:isTablet];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) isConsentOptIn:(CDVInvokedUrlCommand*)command {
    [self pluginLog:[NSString stringWithFormat:@"isConsentOptIn"]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    bool isConsentOptIn = [AATSDK isConsentOptIn];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:isConsentOptIn];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}


- (void) maximumBannerSizePortrait:(CDVInvokedUrlCommand*)command {
    [self pluginLog:[NSString stringWithFormat:@"maximumBannerSizePortrait"]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    AATBannerPlacementSize adType = [AATSDK maximumBannerSizePortrait];
    NSString * size = [self convertAdTypeToString:adType];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:size];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) maximumBannerSizeLandscape:(CDVInvokedUrlCommand*)command {
    [self pluginLog:[NSString stringWithFormat:@"maximumBannerSizeLandscape"]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    AATBannerPlacementSize adType = [AATSDK maximumBannerSizeLandscape];
    NSString * size = [self convertAdTypeToString:adType];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:size];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) fittingBannerSizesPortrait:(CDVInvokedUrlCommand*)command {
    [self pluginLog:[NSString stringWithFormat:@"fittingBannerSizesPortrait"]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    NSSet<NSString*>* adTypes = [AATSDK fittingBannerSizesPortrait];
    NSMutableArray * sizes = [self convertAdTypesToArray:adTypes];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:sizes];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void) fittingBannerSizesLandscape:(CDVInvokedUrlCommand*)command {
    [self pluginLog:[NSString stringWithFormat:@"fittingBannerSizesLandscape"]];
    
    if(![self isAATKitInitialized]) {
        return;
    }
    
    NSSet<NSString*>* adTypes = [AATSDK fittingBannerSizesPortrait];
    NSMutableArray * sizes = [self convertAdTypesToArray:adTypes];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsArray:sizes];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (NSMutableArray *)convertAdTypesToArray:(NSSet<NSString *> *)adTypes {
    NSMutableArray* sizes = [[NSMutableArray alloc] init];
    for(NSString* adType in adTypes) {
        [sizes addObject:adType];
    }
    return sizes;
}

- (NSString *)convertAdTypeToString:(AATBannerPlacementSize)adType {
    NSString* size = @"unknown";
    switch (adType) {
        case AATBannerPlacementSizeBanner320x53:
            size = @"Banner320x53";
            break;
        case AATBannerPlacementSizeBanner320x50:
            size = @"Banner320x50";
            break;
        case AATBannerPlacementSizeBanner375x50:
            size = @"Banner375x50";
            break;
        case AATBannerPlacementSizeBanner390x50:
            size = @"Banner390x50";
            break;
        case AATBannerPlacementSizeBanner414x50:
            size = @"Banner414x50";
            break;
        case AATBannerPlacementSizeBanner428x50:
            size = @"Banner428x50";
        case AATBannerPlacementSizeBanner768x90:
            size = @"Banner768x90";
            break;
        case AATBannerPlacementSizeBanner300x250:
            size = @"Banner300x250";
            break;
        case AATBannerPlacementSizeBanner468x60:
            size = @"Banner468x60";
            break;
        case AATBannerPlacementSizeBanner320x75:
            size = @"Banner320x75";
            break;
        case AATBannerPlacementSizeBanner320x100:
            size = @"Banner320x100";
            break;
        case AATBannerPlacementSizeBanner320x150:
            size = @"Banner320x150";
            break;
        case AATBannerPlacementSizeBanner320x160:
            size = @"Banner320x160";
            break;
        case AATBannerPlacementSizeBanner320x480:
            size = @"Banner320x480";
            break;
        case AATBannerPlacementSizeBanner300x50:
            size = @"Banner300x50";
            break;
    }
    return size;
}

- (void)AATKitObtainedAdRulesFromTheServer:(BOOL)fromTheServer {
    NSString* data = [NSString stringWithFormat:@"{fromTheServer:%@}", fromTheServer?@"true":@"false"];
    NSString* event = [NSString stringWithFormat:@"javascript:cordova.fireDocumentEvent('aatkitObtainedAdRules',%@);", data];
    [self.commandDelegate evalJs:event];
}

- (void)fireEventWithPlacementName:(NSString*)placementName eventName:(NSString*) eventName {
    NSString* data = [NSString stringWithFormat:@"{placementName:'%@'}", placementName];
    NSString* event = [NSString stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@',%@);", eventName, data];
    [self.commandDelegate evalJs:event];
}

- (void)managedConsentNeedsUserInterface:(AATManagedConsent * _Nonnull)managedConsent
{
    managedConsentNeedingUI = managedConsent;
    [self.commandDelegate evalJs:@"javascript:cordova.fireDocumentEvent('managedConsentNeedsUserInterface');"];
}

- (void)managedConsentCMPFinishedWith:(AATManagedConsentState)state {
    NSString* stateParam;
    switch(state) {
        case AATManagedConsentStateObtained:
            stateParam = @"obtained";
            break;
        case AATManagedConsentStateWithheld:
            stateParam = @"withheld";
            break;
        case AATManagedConsentStateCustom:
            stateParam = @"custom";
            break;
        case AATManagedConsentStateUnknown:
        default:
            stateParam = @"unknown";
            break;
    }

    NSString* data = [NSString stringWithFormat:@"{state:'%@'}", stateParam];
    NSString* event = [NSString stringWithFormat:@"javascript:cordova.fireDocumentEvent('managedConsentCMPFinished',%@);", data];
    [self.commandDelegate evalJs:event];
}

- (void)managedConsentCMPFailedToLoad:(AATManagedConsent * _Nonnull)managedConsent with:(NSString * _Nonnull)error {
    [self fireEventWithError:error eventName:@"managedConsentCMPFailedToLoad"];
}

- (void)managedConsentCMPFailedToShow:(AATManagedConsent * _Nonnull)managedConsent with:(NSString * _Nonnull)error {
    [self fireEventWithError:error eventName:@"managedConsentCMPFailedToShow"];
}

- (void)fireEventWithError:(NSString*)error eventName:(NSString*) eventName {
    NSString* data = [NSString stringWithFormat:@"{error:'%@'}", error];
    NSString* event = [NSString stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@',%@);", eventName, data];
    [self.commandDelegate evalJs:event];
}

- (NonIABConsent)getConsentForNetwork:(enum AATAdNetwork)network {
    if([vendorConsentObtainedNetworks containsObject:[NSNumber numberWithInt:network]])
    {
        return NonIABConsentObtained;
    }
    
    return NonIABConsentWithheld;
}

- (NonIABConsent)getConsentForAddapptr {
    return consentForAddapptr;
}

@end
