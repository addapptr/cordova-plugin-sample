#import <Cordova/CDV.h>
#import <AATKit/AATKit.h>

@interface AATKitCordovaPlugin : CDVPlugin <AATDelegate, AATManagedConsentDelegate, AATVendorConsentDelegate>

- (void) initWithConfiguration:(CDVInvokedUrlCommand*)command;
- (void) reconfigure:(CDVInvokedUrlCommand*)command;
- (void) showConsentDialogIfNeeded:(CDVInvokedUrlCommand*)command;
- (void) editConsent:(CDVInvokedUrlCommand*)command;
- (void) reloadConsent:(CDVInvokedUrlCommand*)command;
- (void) isConsentOptIn:(CDVInvokedUrlCommand*)command;
- (void) getVersion:(CDVInvokedUrlCommand*)command;
- (void) setDebugEnabled:(CDVInvokedUrlCommand*)command;
- (void) setDebugShakeEnabled:(CDVInvokedUrlCommand*)command;
- (void) setIsChildDirected:(CDVInvokedUrlCommand*)command;
- (void) getDebugInfo:(CDVInvokedUrlCommand*)command;
- (void) configureDebugScreen:(CDVInvokedUrlCommand*)command;
- (void) setNetworkEnabled:(CDVInvokedUrlCommand*)command;
- (void) isNetworkEnabled:(CDVInvokedUrlCommand*)command;
- (void) addAdNetworkForKeywordTargeting:(CDVInvokedUrlCommand*)command;
- (void) removeAdNetworkForKeywordTargeting:(CDVInvokedUrlCommand*)command;
- (void) setTargetingInfo:(CDVInvokedUrlCommand*)command;
- (void) setTargetingInfoForPlacement:(CDVInvokedUrlCommand*)command;
- (void) setContentTargetingUrl:(CDVInvokedUrlCommand*)command;
- (void) createPlacement:(CDVInvokedUrlCommand*)command;
- (void) createRewardedVideoPlacement:(CDVInvokedUrlCommand*)command;
- (void) createAppOpenAdPlacement:(CDVInvokedUrlCommand*)command;
- (void) setCollapsibleBannerOptions:(CDVInvokedUrlCommand*)command;
- (void) setImpressionListener:(CDVInvokedUrlCommand*)command;
- (void) setPublisherProvidedId:(CDVInvokedUrlCommand*)command;
- (void) reloadPlacement:(CDVInvokedUrlCommand*)command;
- (void) reloadPlacementForced:(CDVInvokedUrlCommand*)command;
- (void) startPlacementAutoReload:(CDVInvokedUrlCommand*)command;
- (void) stopPlacementAutoReload:(CDVInvokedUrlCommand*)command;
- (void) muteVideoAds:(CDVInvokedUrlCommand*)command;
- (void) addPlacementToView:(CDVInvokedUrlCommand*)command;
- (void) removePlacementFromView:(CDVInvokedUrlCommand*)command;
- (void) setPlacementAlignment:(CDVInvokedUrlCommand*)command;
- (void) setPlacementAlignmentWithOffset:(CDVInvokedUrlCommand*)command;
- (void) setPlacementPosition:(CDVInvokedUrlCommand*)command;
- (void) setPlacementContentGravity:(CDVInvokedUrlCommand*)command;
- (void) showPlacement:(CDVInvokedUrlCommand*)command;
- (void) isTablet:(CDVInvokedUrlCommand*)command;
- (void) maximumBannerSizePortrait:(CDVInvokedUrlCommand*)command;
- (void) maximumBannerSizeLandscape:(CDVInvokedUrlCommand*)command;
- (void) fittingBannerSizesPortrait:(CDVInvokedUrlCommand*)command;
- (void) fittingBannerSizesLandscape:(CDVInvokedUrlCommand*)command;

- (void)AATKitObtainedAdRulesFromTheServer:(BOOL)fromTheServer;

- (void)managedConsentNeedsUserInterface:(AATManagedConsent * _Nonnull)managedConsent;
- (void)managedConsentCMPFinishedWith:(NonIABConsent)state;
- (void)managedConsentCMPFailedToLoad:(AATManagedConsent * _Nonnull)managedConsent with:(NSString * _Nonnull)error;
- (void)managedConsentCMPFailedToShow:(AATManagedConsent * _Nonnull)managedConsent with:(NSString * _Nonnull)error;

- (NonIABConsent)getConsentForNetwork:(enum AATAdNetwork)network;
- (NonIABConsent)getConsentForAddapptr;

@end
