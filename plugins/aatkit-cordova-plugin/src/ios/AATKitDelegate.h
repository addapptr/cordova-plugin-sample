#import <AATKit/AATKit.h>
#import <Cordova/CDV.h>

@interface AATKitDelegate : NSObject <AATStickyBannerPlacementDelegate, AATFullscreenPlacementDelegate, AATRewardedVideoPlacementDelegate, AATAppOpenPlacementDelegate>

@property (nonatomic, retain) NSString* placementName;
@property (nonatomic, weak) CDVPlugin* cordovaPlugin;

- (id)initWithPlacementName:(NSString*)placementName andCordovaPlguin:(CDVPlugin*)cordovaPlugin;

@end
