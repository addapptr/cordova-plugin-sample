#import "StatisticsDelegate.h"
#import <AATKit/AATKit.h>

@implementation StatisticsDelegate

-(id)initWithPlacementName:(NSString*)placementName andCordovaPlugin:(CDVPlugin*)cordovaPlugin {
    self = [super init];
    self.placementName = placementName;
    self.cordovaPlugin = cordovaPlugin;
    return self;
}

- (void)AATKitCountedAdSpaceWithPlacement:(id <AATPlacement> _Nullable)placement {
    [self fireEventWithPlacementName:self.placementName eventName:@"countedAdSpace"];
}

- (void)AATKitCountedRequestWithPlacement:(id <AATPlacement> _Nullable)placement for:(enum AATAdNetwork)network {
    NSString* networkString = [StatisticsDelegate convertNetworkToString:network];
    [self fireEventWithPlacementName:self.placementName andNetwork:networkString eventName:@"countedRequest"];
}

- (void)AATKitCountedResponseWithPlacement:(id <AATPlacement> _Nullable)placement for:(enum AATAdNetwork)network {
    NSString* networkString = [StatisticsDelegate convertNetworkToString:network];
    [self fireEventWithPlacementName:self.placementName andNetwork:networkString eventName:@"countedResponse"];
}

- (void)AATKitCountedImpressionWithPlacement:(id <AATPlacement> _Nullable)placement for:(enum AATAdNetwork)network {
    NSString* networkString = [StatisticsDelegate convertNetworkToString:network];
    [self fireEventWithPlacementName:self.placementName andNetwork:networkString eventName:@"countedImpression"];
}

- (void)AATKitCountedVImpressionWithPlacement:(id <AATPlacement> _Nullable)placement for:(enum AATAdNetwork)network {
    NSString* networkString = [StatisticsDelegate convertNetworkToString:network];
    [self fireEventWithPlacementName:self.placementName andNetwork:networkString eventName:@"countedVimpression"];
}

- (void)AATKitCountedNetworkImpressionWithPlacement:(id <AATPlacement> _Nullable)placement for:(enum AATAdNetwork)network {
    NSString* networkString = [StatisticsDelegate convertNetworkToString:network];
    [self fireEventWithPlacementName:self.placementName andNetwork:networkString eventName:@"countedNimpression"];
}

- (void)AATKitCountedClickWithPlacement:(id <AATPlacement> _Nullable)placement for:(enum AATAdNetwork)network {
    NSString* networkString = [StatisticsDelegate convertNetworkToString:network];
    [self fireEventWithPlacementName:self.placementName andNetwork:networkString eventName:@"countedClick"];
}

- (void)AATKitCountedDirectDealImpressionWithPlacement:(id <AATPlacement> _Nullable)placement for:(enum AATAdNetwork)network {
    NSString* networkString = [StatisticsDelegate convertNetworkToString:network];
    [self fireEventWithPlacementName:self.placementName andNetwork:networkString eventName:@"countedDirectDealImpression"];
}

- (void)AATKitCountedMediationCycleWithPlacement:(id <AATPlacement> _Nullable)placement {
    [self fireEventWithPlacementName:self.placementName eventName:@"countedMediationCycle"];
}

+(NSString*) convertNetworkToString:(int) network; {
    NSString* networkString = @"UNKNOWN";
    
    if(network == AATAdNetworkADCOLONY) {
        networkString = @"ADCOLONY";
    }
    else if(network == AATAdNetworkADMOB) {
        networkString = @"ADMOB";
    }
    else if(network == AATAdNetworkRTB2) {
        networkString = @"RTB2";
    }
    else if(network == AATAdNetworkAMAZONHB) {
        networkString = @"AMAZONHB";
    }
    else if(network == AATAdNetworkAPPLOVIN) {
        networkString = @"APPLOVIN";
    }
    else if(network == AATAdNetworkAPPLOVINMAX) {
        networkString = @"APPLOVINMAX";
    }
    else if(network == AATAdNetworkAPPNEXUS) {
        networkString = @"APPNEXUS";
    }
    else if(network == AATAdNetworkBLUESTACK) {
        networkString = @"BLUESTACK";
    }
    else if(network == AATAdNetworkCRITEOSDK) {
        networkString = @"CRITEOSDK";
    }
    else if(network == AATAdNetworkDFP) {
        networkString = @"DFP";
    }
    else if(network == AATAdNetworkDFPDIRECT) {
        networkString = @"DFPDIRECT";
    }
    else if(network == AATAdNetworkFACEBOOK) {
        networkString = @"FACEBOOK";
    }
    else if(network == AATAdNetworkFEEDAD) {
        networkString = @"FEEDAD";
    }
    else if(network == AATAdNetworkGRAVITERTB) {
        networkString = @"GRAVITERTB";
    }
    else if(network == AATAdNetworkINMOBI) {
        networkString = @"INMOBI";
    }
    else if(network == AATAdNetworkIRONSOURCE) {
        networkString = @"IRONSOURCE";
    }
    else if(network == AATAdNetworkMINTEGRAL) {
        networkString = @"MINTEGRAL";
    }
    else if(network == AATAdNetworkOGURY) {
        networkString = @"OGURY";
    }
    else if(network == AATAdNetworkPUBNATIVE) {
        networkString = @"PUBNATIVE";
    }
    else if(network == AATAdNetworkRUBICON) {
        networkString = @"RUBICON";
    }
    else if(network == AATAdNetworkSMAATO) {
        networkString = @"SMAATO";
    }
    else if(network == AATAdNetworkSMARTAD) {
        networkString = @"SMARTAD";
    }
    else if(network == AATAdNetworkSMARTADSERVERDIRECT) {
        networkString = @"SMARTADSERVERDIRECT";
    }
    else if(network == AATAdNetworkTAPPX) {
        networkString = @"TAPPX";
    }
    else if(network == AATAdNetworkTEADS) {
        networkString = @"TEADS";
    }
    else if(network == AATAdNetworkVUNGLE2) {
        networkString = @"VUNGLE2";
    }
    else if(network == AATAdNetworkUNITY) {
        networkString = @"UNITY";
    }
    
    return networkString;
}

- (void)fireEventWithPlacementName:(NSString*)placementName eventName:(NSString*) eventName {
    NSString* data = [NSString stringWithFormat:@"{placementName:'%@'}", placementName];
    NSString* event = [NSString stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@',%@);", eventName, data];
    [self.cordovaPlugin.commandDelegate evalJs:event];
}

- (void)fireEventWithPlacementName:(NSString*)placementName andNetwork:(NSString*)network eventName:(NSString*) eventName {
    NSString* data = [NSString stringWithFormat:@"{placementName:'%@', network:'%@'}", placementName, network];
    NSString* event = [NSString stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@',%@);", eventName, data];
    [self.cordovaPlugin.commandDelegate evalJs:event];
}

@end
