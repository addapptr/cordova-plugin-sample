var app = {
    var:bannerAutoreloadEnabled = false, 
    var:bannerAlignment = 1,
    var:fullscreenAutoreloadEnabled = false, 

    // Application Constructor
    initialize: function() {
        document.getElementById("initializeAATKit").addEventListener("click", this.initializeAATKit.bind(this));
        document.getElementById("reloadBannerPlacement").addEventListener("click", this.reloadBannerPlacement.bind(this));
        document.getElementById("autoreloadBannerPlacement").addEventListener("click", this.autoreloadBannerPlacement.bind(this));
        document.getElementById("changeBannerAlignment").addEventListener("click", this.changeBannerAlignment.bind(this));
        document.getElementById("reloadFullscreenPlacement").addEventListener("click", this.reloadFullscreenPlacement.bind(this));
        document.getElementById("autoreloadFullscreenPlacement").addEventListener("click", this.autoreloadFullscreenPlacement.bind(this));
        document.getElementById("showFullscreenPlacement").addEventListener("click", this.showFullscreenPlacement.bind(this));

        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);

        document.addEventListener('aatkitHaveAd', function(data){ console.log("[AATKit Event] aatkitHaveAd: " + data.placementName);});
        document.addEventListener('aatkitNoAd', function(data){console.log("[AATKit Event] aatkitNoAd: " + data)});
        document.addEventListener('aatkitPauseForAd', function(data){console.log("[AATKit Event] aatkitPauseForAd: " + data)});
        document.addEventListener('aatkitResumeAfterAd', function(data){console.log("[AATKit Event] aatkitResumeAfterAd: " + data)});
        document.addEventListener('aatkitShowingEmpty', function(data){console.log("[AATKit Event] aatkitShowingEmpty: " + data)});
        document.addEventListener('aatkitUserEarnedIncentive', function(data){console.log("[AATKit Event] aatkitUserEarnedIncentive: " + data)});
        document.addEventListener('aatkitObtainedAdRules', function(){console.log("[AATKit Event] aatkitObtainedAdRules")});
        document.addEventListener('aatkitUnknownBundleId', function(){console.log("[AATKit Event] aatkitUnknownBundleId" )});
        document.addEventListener('aatkitHaveAdForPlacementWithBannerView', function(data){console.log("[AATKit Event] aatkitHaveAdForPlacementWithBannerView: " + data)});
        document.addEventListener('aatkitHaveVASTAd', function(data){console.log("[AATKit Event] aatkitHaveVASTAd: " + data)});

        document.addEventListener('onManagedConsentCompletion', function(){console.log("[AATKit Event] onManagedConsentCompletion js delegate")});
        document.addEventListener('managedConsentNeedsUserInterface', function(){
            console.log("[AATKit Event] managedConsentNeedsUserInterface js delegate");
            AATKitCordova.showConsentDialogIfNeeded();
        });

        document.addEventListener('countedAdSpace', function(data){console.log("[AATKit Statistics Event] countedAdSpace palcementName: " + data.placementName)});
        document.addEventListener('countedRequest', function(data){console.log("[AATKit Statistics Event] countedRequestForNetwork palcementName: " + data.placementName + " network: " + data.network)});
        document.addEventListener('countedResponse', function(data){console.log("[AATKit Statistics Event] countedResponseForNetwork palcementName: " + data.placementName + " network: " + data.network)});
        document.addEventListener('countedImpression', function(data){console.log("[AATKit Statistics Event] countedImpressionForNetwork palcementName: " + data.placementName + " network: " + data.network)});
        document.addEventListener('countedVimpression', function(data){console.log("[AATKit Statistics Event] countedVimpressionForNetwork palcementName: " + data.placementName + " network: " + data.network)});
        document.addEventListener('countedClick', function(data){console.log("[AATKit Statistics Event] countedClickForNetwork palcementName: " + data.placementName + " network: " + data.network)});

        document.addEventListener('didCountImpression', function(data){
            console.log("[AATKit Statistics Event Impression] didCountImpression");
            console.log("[AATKit Statistics Event Impression] didCountImpression palcementName: " + data.placementName + " adNetwork: " + data.adNetwork);
        });
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function() {
    },

    initializeAATKit: function() {
        AATKitCordova.setDebugEnabled(true);
        AATKitCordova.initWithConfiguration({
            "testModeAccountId": 74,
            "useGeoLocation": false,
            "consentRequired": true,
            "consent": {
                "type": "managedCMPGoogle"
            },
            "networkOptions": {
                "appNexusOptions": {
                    "autoCloseTime": 10,
                    "supportVideoBanner": true
                },
                "dfpOptions": {
                    "inlineBannerMaxHeight": 50
                }
            }
        });

        AATKitCordova.createPlacement("stickyBanner", "Banner320x53");
        AATKitCordova.addPlacementToView("stickyBanner");
        AATKitCordova.setPlacementAlignment("stickyBanner", "TopCenter");
        AATKitCordova.setPlacementContentGravity("stickyBanner","Center");

        AATKitCordova.createPlacement("fullscreenAd", "Fullscreen");

        AATKitCordova.setImpressionListener("stickyBanner");
        AATKitCordova.setImpressionListener("fullscreenAd");

        AATKitCordova.setCollapsibleBannerOptions("stickyBanner", {
            "position": "bottom",
            "minDelayInSeconds": 10
        });

        AATKitCordova.configureDebugScreen({
            "title": "AATKit Cordova Sample App Debug Screen",
            "showAdvertisingId": false
        });
    },


    reloadBannerPlacement: function() {
        console.log("[AATKitTest] calling...");

        AATKitCordova.isTablet((isTablet) => {
            console.log("[AATKitTest] isTablet " + isTablet);
        });
        AATKitCordova.maximumBannerSizePortrait((maximumBannerSizePortrait) => {
            console.log("[AATKitTest] maximumBannerSizePortrait " + maximumBannerSizePortrait);
        });
        AATKitCordova.maximumBannerSizeLandscape((maximumBannerSizeLandscape) => {
            console.log("[AATKitTest] maximumBannerSizeLandscape " + maximumBannerSizeLandscape);
        });
        AATKitCordova.fittingBannerSizesPortrait((fittingBannerSizesPortrait) => {
            console.log("[AATKitTest] fittingBannerSizesPortrait " + fittingBannerSizesPortrait);
        });
        AATKitCordova.fittingBannerSizesLandscape((fittingBannerSizesLandscape) => {
            console.log("[AATKitTest] fittingBannerSizesLandscape " + fittingBannerSizesLandscape);
        });

        AATKitCordova.setTargetingInfo({"one" : ["a","b"], "three" : ["c","d","e"]});
        AATKitCordova.setTargetingInfoForPlacement("stickyBanner", {"one" : ["a","b"], "three" : ["c","d","e"]});

        AATKitCordova.setContentTargetingUrl("https://xxxx.yyy.com");
        AATKitCordova.setContentTargetingUrlForPlacement("stickyBanner", "https://xxxx.yyy.com");

        AATKitCordova.muteVideoAds(true);

        AATKitCordova.createAppOpenAdPlacement("openPlacement");
    },

    autoreloadBannerPlacement: function() {
        if(bannerAutoreloadEnabled) {
            AATKitCordova.stopPlacementAutoReload("stickyBanner");
        } else {
            AATKitCordova.startPlacementAutoReload("stickyBanner");
        }

        bannerAutoreloadEnabled = !bannerAutoreloadEnabled;
    },

    changeBannerAlignment: function() {
        bannerAlignment++;
        if(bannerAlignment > 5) {
            bannerAlignment = 0;
        }
        
        if(bannerAlignment == 0) {
            AATKitCordova.setPlacementAlignment("stickyBanner", "TopLeft");
        } else if(bannerAlignment == 1) {
            AATKitCordova.setPlacementAlignment("stickyBanner", "TopCenter");
        } else if(bannerAlignment == 2) {
            AATKitCordova.setPlacementAlignment("stickyBanner", "TopRight");
        } else if(bannerAlignment == 3) {
            AATKitCordova.setPlacementAlignment("stickyBanner", "BottomLeft");
        } else if(bannerAlignment == 4) {
            AATKitCordova.setPlacementAlignment("stickyBanner", "BottomCenter");
        } else {
            AATKitCordova.setPlacementAlignment("stickyBanner", "BottomRight");
        }
    },

    reloadFullscreenPlacement: function() {
        AATKitCordova.reloadPlacement("fullscreenAd");
    },

    autoreloadFullscreenPlacement: function() {
        if(fullscreenAutoreloadEnabled) {
            AATKitCordova.stopPlacementAutoReload("fullscreenAd");
        } else {
            AATKitCordova.startPlacementAutoReload("fullscreenAd");
        }

        fullscreenAutoreloadEnabled = !fullscreenAutoreloadEnabled;
    },

    showFullscreenPlacement: function() {
        AATKitCordova.showPlacement("fullscreenAd", (interstitialShown) => {
            console.log("interstitialShown " + interstitialShown);
        });
    },
};

app.initialize();